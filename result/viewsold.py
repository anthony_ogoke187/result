from django.contrib.auth.models import User, Group
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from django.urls import reverse
from academic.models import ClassRegistration
from .forms import SubjectRegistrationForm, ClassSelectMarkEntryForm, ClassSelectSubjectListForm, ClassSelectMarkEntryForm2
from .models import SubjectRegistration
from student.models import AcademicInfo
from django.shortcuts import render, get_object_or_404, redirect
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse, Http404
from django.conf import settings
from django.contrib import messages
from.forms import *
from.models import *
# from student.models import *
from teacher.models import *
from student.models import AcademicInfo, PersonalInfo
from administration.models import *
from academic.models import *
from django.contrib.auth.decorators import permission_required
#import xlwt
from django.contrib.auth.decorators import login_required




from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags

import datetime
from io import BytesIO
from reportlab.pdfgen import canvas

import time

import logging


logger = logging.getLogger(__name__)

def add_subject(request):
    form  = SubjectRegistrationForm()
    if request.method == 'POST':
        form = SubjectRegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('subject-list')
    context = {'form': form}
    return render(request, 'result/add-subject.html', context)

def subject_list(request):
    form = ClassSelectSubjectListForm(request.GET or None)
    select_class = request.GET.get('select_class', None)
    if select_class:
        cls = ClassRegistration.objects.get(id=select_class)
        subjects = SubjectRegistration.objects.filter(select_class=cls)
        context = {'form': form, 'subjects': subjects}
        return render(request, 'result/subject-list.html', context)

    context = {'form': form}
    return render(request, 'result/subject-list.html', context)

def mark_entry1(request):
    form = ClassSelectMarkEntryForm(request.GET or None)
    select_class = request.GET.get('select_class', None)
    if select_class:
        cls = ClassRegistration.objects.get(id=select_class)
        student = AcademicInfo.objects.filter(class_info=cls)
        context = {'form': form, 'student': student}
        return render(request, 'result/mark-entry.html', context)
    context = {'form': form}
    return render(request, 'result/mark-entry.html', context)




def mark_entry2(request):
    form = ClassSelectMarkEntryForm2(request.GET or None)
    select_class = request.GET.get('select_class', None)
    if select_class:
        cls = ClassRegistration.objects.get(id=select_class)
        student = AcademicInfo.objects.filter(class_info=cls)
        context = {'form': form, 'student': student}
        return render(request, 'result/mark-entry.html', context)
    context = {'form': form}
    return render(request, 'result/mark-entry.html', context)










@login_required(login_url='administration:login')
def mark_entry(request):
    print("whyyyyyyyyyyyyy")
    # global response

    group_students = []
    try:
        result = Result.objects.filter(user=request.user)[0]
    except:
        result = None
    if result:
        year = result.year
        level = result.clas
    else:
        year = None
        level = None
    group_student = User.objects.filter(groups__name='Students')
    print("group students")
    print(group_student)
    group_admin = Group.objects.get(name="Admin")
    for i in group_student:
        group_students.append(i.username)

    form = PinCheckForm()
    context = {
        'result': result,
        'form': form,
        'group_students':group_students,
        'year': year,
        'level': level,
    }
    return render(request, 'result/mark-entry.html', context)




@login_required(login_url='administration:login')
def mark_entry_mid_term(request):
    print("whyyyyyyyyyyyyy")
    # global response

    group_students = []
    try:
        result = Result.objects.filter(user=request.user)[0]
    except:
        result = None
    if result:
        year = result.year
        level = result.clas
    else:
        year = None
        level = None
    group_student = User.objects.filter(groups__name='Students')
    print("group students")
    print(group_student)
    group_admin = Group.objects.get(name="Admin")
    for i in group_student:
        group_students.append(i.username)

    form = PinCheckForm2()
    context = {
        'result': result,
        'form': form,
        'group_students':group_students,
        'year': year,
        'level': level,
    }
    return render(request, 'result/mark-entry.html', context)



def download_write_pdf_view(request, level, term, year):
    personal_info = PersonalInfo.objects.get(user=request.user)
    print(personal_info)
    level1 = ClassInfo.objects.get(name=level)
    term1 = Term.objects.get(term=term)
    year1 = Year.objects.get(time=year)
    result = Result.objects.get(user=request.user, clas=level1, term=term1, year=year1)
    print(request.user.last_name)
    print(request.user.first_name)
    mypdf = result.pdf
    response = HttpResponse(mypdf, content_type='application/pdf')
    response['Content-Disposition'] = f'attachment; filename={personal_info.last_name.capitalize()}' + f'_{personal_info.first_name.capitalize()}_' + f'{result.term}_result' + '.pdf'
    return response


def print_write_pdf_view(request, level, term, year):
    personal_info = PersonalInfo.objects.get(user=request.user)
    level1 = ClassInfo.objects.get(name=level)
    term1 = Term.objects.get(term=term)
    year1 = Year.objects.get(time=year)
    result = Result.objects.get(user=request.user, clas=level1, term=term1, year=year1)
    mypdf = result.pdf
    response = HttpResponse(mypdf, content_type='application/pdf')
    response['Content-Disposition'] = f'inline; filename={personal_info.last_name.capitalize()}' + f'__{personal_info.first_name.capitalize()}__' + f'__{result.term}_result' + '.pdf'
    return response

def pin_check(request):
    username = request.user.username
    group_students = []
    user = User.objects.get(username=username)
    #personal_info = AcademicInfo.objects.get(registration_no=username).class_info.name
    level = AcademicInfo.objects.get(registration_no=username).class_info.name
    group_student = User.objects.filter(groups__name='Students')
    for i in group_student:
        group_students.append(i.username)
    try:
        result = Result.objects.filter(user=user)[0]
    except:
        result = None

    form = PinCheckForm()



    current_term1 = CurrentTerm.objects.all()[0].name
    current_term = Term.objects.get(term=current_term1)

    current_session1 = CurrentSession.objects.all()[0].name
    current_session = Year.objects.get(time=current_session1)




    try:
        result = Result.objects.get(user=request.user, year=current_session, term=current_term)
    except:
        result = None



    #if request.is_ajax and request.method == "POST":
    if request.method == "POST":
        forms = PinCheckForm(request.POST)
        if forms.is_valid():
            if result != None:
                year = Year.objects.get(time=current_session1)
                term = forms.cleaned_data['term']
                pin = forms.cleaned_data['pin']
                try:
                    pin_available = PinCode.objects.get(reference=pin)
                except ObjectDoesNotExist:
                    pin_available = None
                if pin_available:
                    try:
                        expire = pin_available.expiry_date

                        #check if the days left is greater or equal to the post's duration
                        if expire <= datetime.datetime.now():

                            #if duration exceeded delete post
                            pin_available.delete()
                            messages.success(request, "Pin expired", extra_tags='error')
                            return HttpResponseRedirect(reverse('result:mark-entry'))
                        else:

                            check_count = PinCodeTime.objects.filter(reference=pin).count()
                            if check_count > 0:
                                check = PinCodeTime.objects.filter(reference=pin)
                                checking = PinCodeTime.objects.filter(reference=pin)[0]
                            else:
                                check = None
                    except ObjectDoesNotExist:
                        check = None
                    if check:
                        print(check)
                        if username == checking.user.username:
                            #fetch the requested result
                            level1 = ClassInfo.objects.get(name=level)
                            term1 = Term.objects.get(term=term)
                            year1 = Year.objects.get(time=year)
                            try:
                                result = Result.objects.get(user=request.user, clas=level1, term=term1, year=year1)
                            except:
                                result = None

                            if result != None:
                                if result == checking.result: #note thatperformance will not need [0] when it is a single object after using get on comment above
                                    if check_count <= 4:
                                        create_pin = PinCodeTime(reference=pin, user=request.user, result=result) #include Performcae object when creating PineCodeTime
                                        create_pin.save()
                                        context = {
                                            'result': result
                                        }
                                        #messages.success(request, "login successful.", extra_tags='success')
                                        # html = render_to_string('result/mark-entry1.html', context, request=request)
                                        # return JsonResponse({'form': html})
                                        return render(request, 'result/mark-entry1.html', context)
                                    else:
                                        pin_available.delete()
                                        check = PinCodeTime.objects.filter(reference=pin)
                                        for i in check:
                                            i.delete()
                                        messages.success(request, "Pin use completed", extra_tags='error')
                                        return HttpResponseRedirect(reverse('result:mark-entry'))
                                #else: return error saying wrong result for pin.
                                else:
                                    messages.success(request, f"Pin already registered for {checking.result.term.term} result", extra_tags='error')
                                    return HttpResponseRedirect(reverse('result:mark-entry'))
                            else:
                                messages.success(request, "Result not available", extra_tags='error')
                                return HttpResponseRedirect(reverse('result:mark-entry'))
                        else:
                            messages.success(request, "Pin already registered to a different student", extra_tags='error')
                            return HttpResponseRedirect(reverse('result:mark-entry'))
                    else:
                        #feth the requested performance result using term, year, class and user
                        #fetch the requested result
                        level1 = ClassInfo.objects.get(name=level)
                        term1 = Term.objects.get(term=term)
                        year1 = Year.objects.get(time=year)

                        try:
                            result = Result.objects.get(user=request.user, clas=level1, term=term1, year=year1)

                        except:
                            result = None

                        if result != None:
                            create_pin = PinCodeTime(reference=pin, user=user, result=result) #include Performcae object when creating PineCodeTime
                            create_pin.save()
                            context = {
                                'result': result,
                                'group_students':group_students,
                            }
                            #messages.success(request, "login successful.", extra_tags='success')
                            # html = render_to_string('result/mark-entry1.html', context, request=request)
                            # return JsonResponse({'form': html}, safe=False)
                            return render(request, 'result/mark-entry1.html', context)
                        else:
                            messages.success(request, "Result not available yet3", extra_tags='error')
                            return HttpResponseRedirect(reverse('result:mark-entry'))
                else:
                    messages.success(request, "wrong pin", extra_tags='error')
                    return redirect('result:mark-entry')
            else:
                messages.success(request, "Result not availabe", extra_tags='error')
                return redirect('result:mark-entry')

        else:
            messages.success(request, "Invalid login details", extra_tags='error')
            return HttpResponseRedirect(reverse('result:mark-entry'))
    else:
        forms = PinCheckForm()

    context = {
        'result': result,
        'form': form,
        'group_students':group_students,
    }
    return render(request, 'result/mark-entry.html', context)



@login_required(login_url='administration:login')
def student_mid_term_result(request, reg_no):
    academic_info = AcademicInfo.objects.get(registration_no=reg_no)

    user = academic_info.personal_info.user
    clas = academic_info.class_info.name


    try:
        current_term1 = CurrentTerm.objects.all()[0].name
        current_term = Term.objects.get(term=current_term1)
    except:
        current_term = None
        current_term1 = None


    try:
        current_session1 = CurrentSession.objects.all()[0].name
        current_session = Year.objects.get(time=current_session1)
    except:
        current_session = None
        current_session1 = None




    try:
        result = Result.objects.get(user=user, year=current_session, term=current_term)
    except:
        result = None


    url = '/student/student-result/' + reg_no+'/'



    if result != None:
        context = {
            'result': result,
            'clas': clas,
            'url': url,
            'reg_no': reg_no
        }
        return render(request, 'result/mark-entry1.html', context)
    else:
        messages.success(request, "Result not availabe", extra_tags='error')
        return redirect('student-list-re', username=clas)



@login_required(login_url='administration:login')
def home_page(request):
    # global response
    group_students = []
    username = request.user
    user = User.objects.get(username=username)
    group_student = Group.objects.get(name="Students")
    print(group_student)
    group_admin = Group.objects.get(name="Admin")
    for i in group_students:
        group_students.append(i)
        print(i)
    try:
        result = Result.objects.filter(user=user)[0]
    except:
        result = None

    print(result)

    # path = result.pdf.url
    # img = pdf2image.convert_from_path(path)
    # for i,image in enumerate(img):
    #     image.save(response, "png")

    context = {
        'result': result,
        'group_student':group_student,
        # 'img':response
    }
    #return render(request, 'administration/dashboard.html', context)
    return render(request, 'home.html', context)



@login_required(login_url='administration:login')
def result_preview(request):
    # global response

    if request.is_ajax and request.method == "POST":
        username = request.user
        user = User.objects.get(username=username)
        try:
            result = Result.objects.filter(user=user)[0]
        except:
            result = None

        # html = render_to_string('rscheck/table.html', context, request=request)
        html = render_to_string('result/mark-entry1.html', request=request)
        return JsonResponse({'form': html})

    context = {
        'result': result
        # 'img':response
    }
    #return render(request, 'administration/dashboard.html', context)
    return render(request, 'result/mark-entry.html', context)




def mark_table(request, subject):
    result-preview
    return render(request, 'result/mark-table.html')
