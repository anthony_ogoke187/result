from django.urls import path, re_path

from .views import *


app_name = 'result'


urlpatterns = [
    path('add-subject', add_subject, name='add-subject'),
    path('subject-list', subject_list, name='subject-list'),
    path('mark-entry', mark_entry, name='mark-entry'),
    #path('mark-entry-mid-term', mark_entry_mid_term, name='mark-entry-mid-term'),
    path('result-preview', pin_check, name='result-preview'),
    #path('result-preview-mid-term', pin_check_mid_term, name='result-preview-mid-term'),
    re_path(r'mid-term-result/(?P<reg_no>[\w/]+)/$', student_mid_term_result, name="student-mid-term-result"),
    re_path(r'print/(?P<level>[\w-]+)/(?P<term>[\w\s\w]+)/(?P<year>[\w/]+)/$', print_write_pdf_view, name="print"),
    re_path(r'download/(?P<level>[\w-]+)/(?P<term>[\w\s\w]+)/(?P<year>[\w/]+)/$', download_write_pdf_view, name="download"),
    #path('print/<level>/<term>/<year>', print_write_pdf_view, name='print'),
    #path('download/<level>/<term>/<year>', download_write_pdf_view, name='download'),
    path('mark-table/<subject>', mark_table, name='mark-table'),

]
