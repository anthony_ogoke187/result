from django import forms
from .models import SubjectRegistration, ClassRegistration

from django.contrib.auth.models import User

from administration.models import *




# class UserLoginForm(forms.Form):
#     username = forms.CharField(label="", widget= forms.TextInput(attrs = {'placeholder':'Username'}))
#     password = forms.CharField(label="", widget=forms.PasswordInput(attrs = {'placeholder':'Pin'}))


class PinCheckForm(forms.ModelForm):
    pin = forms.CharField(label="", widget=forms.PasswordInput())

    class Meta:
        model = Administration
        fields = ('term',)



class PinCheckForm2(forms.ModelForm):

    class Meta:
        model = Administration
        fields = ('term',)





class NumberOfPin(forms.ModelForm):
    class Meta:
        model = Number
        fields = (
            'numbers',
            )




class YearForm(forms.Form):
    username = forms.CharField(label="", widget= forms.TextInput(attrs = {'placeholder':'Username'}))
    password = forms.CharField(label="", widget=forms.PasswordInput(attrs = {'placeholder':'Pin'}))


class StudentsLevelForm(forms.ModelForm):
    class Meta:
        model = StudentsInClass
        fields = ('level',)





class ClassLevelForm(forms.ModelForm):
    class Meta:
        model = ClassLevel
        fields = ('class_level',)


class AdministrationForm(forms.ModelForm):
    class Meta:
        model = Administration
        fields = ('term',)


# class TermForm(forms.ModelForm):
#     class Meta:
#         model = Performance
#         fields = ('term',)


# class YearForm(forms.ModelForm):
#     class Meta:
#         model = Performance
#         fields = ('year',)

# class PerformanceForm(forms.ModelForm):
#     class Meta:
#         model = Performance
#         fields = ('year', 'pdf', 'user', 'clas', 'term',)


# class UpdateForm(forms.ModelForm):
#     class Meta:
#         model = Performance
#         fields = ('year', 'pdf', 'user', 'clas', 'term',)





class SubjectRegistrationForm(forms.ModelForm):
    class Meta:
        model = SubjectRegistration
        fields = '__all__'

        widgets = {
            'select_class': forms.Select(attrs={'class': 'form-control'}),
            'subject_name': forms.TextInput(attrs={'class': 'form-control'}),
            'subject_code': forms.NumberInput(attrs={'class': 'form-control'}),
            'marks': forms.NumberInput(attrs={'class': 'form-control'}),
            'pass_mark': forms.NumberInput(attrs={'class': 'form-control'}),
        }

class ClassSelectSubjectListForm(forms.Form):
    select_class = forms.ModelChoiceField(queryset=ClassRegistration.objects.all(), widget=forms.Select(attrs={'class': 'form-control'}))


class ClassSelectMarkEntryForm(forms.Form):
    select_class = forms.ModelChoiceField(queryset=ClassRegistration.objects.all(), widget=forms.Select(attrs={'class': 'form-control'}))



class ClassSelectMarkEntryForm2(forms.Form):
    select_class = forms.ModelChoiceField(queryset=ClassRegistration.objects.all(), widget=forms.Select(attrs={'class': 'form-control'}))
