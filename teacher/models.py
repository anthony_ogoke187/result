from django.db import models
from django.contrib.auth.models import User
from academic.models import Department
from administration.models import *
from address.models import District, Upazilla, Union
from academic.models import ClassInfo


from django.template.loader import render_to_string
from django.utils.safestring import mark_safe
from django.db.models.fields import BLANK_CHOICE_DASH
from django.core.validators import FileExtensionValidator
import uuid
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.utils.text import slugify



class EducationInfo(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="teacher_education", null=True, blank=True,)
    name_of_degree = models.CharField(max_length=100, null=True, blank=True,)
    institute = models.CharField(max_length=255, null=True, blank=True,)
    passing_year_date = models.DateField(null=True, blank=True,)
    grade = models.FloatField(max_length=45, null=True, blank=True,)
    certificate = models.FileField(upload_to='documents/', null=True, blank=True,)
    upload_cv = models.FileField(upload_to='documents/', null=True, blank=True,)


    def __str__(self):
        return self.user.username




class PersonalInfo(models.Model):
    #name = models.CharField(max_length=45)
    first_name = models.CharField(max_length=45, null=True, blank=True,)
    last_name = models.CharField(max_length=45, null=True, blank=True,)
    pass_word = models.CharField(max_length=45, null=True, blank=True,)
    employee_code = models.CharField(max_length=45, null=True, blank=True, unique=True)
    user = models.ForeignKey(User, default='', on_delete=models.CASCADE, related_name="user_teacher",)
    photo = models.ImageField(null=True, blank=True,)
    classes_assigned = models.CharField(max_length=1500, null=True, blank=True,)
    date_of_birth = models.DateField(blank=True, null=True,)
    date_employed= models.DateField(blank=True, null=True,)
    education = models.ForeignKey(EducationInfo, on_delete=models.CASCADE, blank=True, null=True)
    nationality_choice = (
        ('Ghana', 'Ghana'),
        ('Nigeria', 'Nigeria')
    )
    nationality = models.CharField(max_length=45, blank=True, null=True, choices=nationality_choice)
    religion_choice = (
        ('Islam', 'Islam'),
        ('Christianity', 'Christianity')
    )
    religion = models.CharField(max_length=45, blank=True, null=True, choices=religion_choice)
    gender_choice = (
        ('male', 'Male'),
        ('female', 'Female')
    )
    gender = models.CharField(choices=gender_choice, blank=True, null=True, max_length=10)
    driving_license_passport = models.IntegerField(unique=True, blank=True, null=True,)
    phone_no = models.CharField(max_length=11, unique=True, blank=True, null=True,)
    email = models.CharField(max_length=255, blank=True, null=True,)
    marital_status_choice = (
        ('married', 'Married'),
        ('widowed', 'Widowed'),
        ('separated', 'Separated'),
        ('divorced', 'Divorced'),
        ('single', 'Single')
    )
    marital_status = models.CharField(choices=marital_status_choice, max_length=10,blank=True, null=True)
    address = models.CharField(max_length=2000, blank=True, null=True)
    is_delete = models.BooleanField(default=False)
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"


class ClassAssignedToTeacher(models.Model):
    name = models.CharField(max_length=45, null=True, blank=True,)
    personal_info = models.ForeignKey(PersonalInfo, on_delete=models.CASCADE, null=True, blank=True, related_name="teacher_persona")
    user = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE, related_name='classes_assigned_teacher')

    class Meta:
        verbose_name = 'Class Assigned To Teacher'
        verbose_name_plural = 'Class Assigned To Teacher'

    def __str__(self):
        return self.name


class ExcelUpload(models.Model):
    excell_sheet = models.FileField(null=True, blank=True)