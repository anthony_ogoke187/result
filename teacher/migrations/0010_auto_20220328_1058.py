# Generated by Django 3.0.7 on 2022-03-28 10:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('teacher', '0009_auto_20220328_1036'),
    ]

    operations = [
        migrations.AlterField(
            model_name='excelupload',
            name='excell_sheet',
            field=models.FileField(blank=True, null=True, upload_to='xlx_teachers/'),
        ),
    ]
