from django.urls import path, re_path

from . import views

urlpatterns = [
    path('registration', views.teacher_registration, name='teacher-registration'),
    path('list', views.teacher_list, name='teacher-list'),
    path('teacher-upload/', views.excell_teacher_registration, name='excell_teacher_registration'),
    path('list-search', views.teacher_list_search, name='teacher-list-search'),
    path('teachers-excel-format', views.export_teachers_registration_xls, name='export_teachers_registration_xls'),
    path('create-classroom/', views.create_class, name='create-classroom'),
    path('create-classroom-edit/', views.create_class_edit, name='create-classroom-edit'),
    re_path(r'password_change1/(?P<username>[\w/]+)/$', views.change_password, name="password_change1"),
    re_path(r'teacher-assign-assign/(?P<username>[\w/]+)/$', views.teacher_assign1, name="teacher-assign-assign"),
    re_path(r'teacher-assign/(?P<username>[\w/]+)/$', views.teacher_assign, name="teacher-assign"),
    re_path(r'teacher-edit-edit/(?P<username>[\w/]+)/$', views.teacher_edit, name="teacher-edit-edit"),
    re_path(r'make-admin/(?P<username>[\w/]+)/$', views.make_admin, name="make-admin"),
    re_path(r'remove-admin/(?P<username>[\w/]+)/$', views.remove_admin, name="remove-admin"),
    re_path(r'admin-edit/(?P<username>[\w/]+)/$', views.admin_edit, name="admin-edit"),
    path('deleted-teachers', views.deleted_teachers, name='deleted-teachers'),
    re_path(r'deleted-teachers/(?P<username>[\w/_]+)/$', views.deleted_teachers, name="deleted-teachers-re"),
    re_path(r'restore-teachers-re/(?P<username>[\w/_]+)/$', views.restore_teachers_re, name="restore-teachers-re"),
    #re_path(r'teacher-edit-assign/(?P<username>[\w/]+)/$', views.teacher_edit_assign, name="teacher-edit-assign"),
    re_path(r'teacher-assign-delete/(?P<username>[\w/]+)/$', views.teacher_assign_delete, name="teacher-assign-delete"),
    #re_path(r'create-classroom-edit/(?P<class_name>[\w/]+)/', views.create_class_edit, name="create-classroom-edit"),
    path('profile/<teacher_id>', views.teacher_profile, name='teacher-profile'),
    re_path(r'deleted-teachers/(?P<username>[\w/_]+)/$', views.deleted_teachers, name="deleted-teachers-re"),
    #path('delete/<teacher_id>', views.teacher_delete, name='teacher-delete'),
    re_path(r'teacher-delete/(?P<username>[\w/_]+)/$', views.teacher_delete, name="teacher-delete"),
    path('edit/<teacher_id>', views.teacher_edit, name='teacher-edit'),
    path('load-upazilla', views.load_upazilla, name='load-upazilla'),
    re_path(r'classroom-delete/(?P<class_name>[\w]+)/$', views.create_class_delete, name="classroom-delete"),
]
