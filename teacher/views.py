from django.shortcuts import render, redirect

from django.db.models import Count, Q

from . import forms
from .forms import *
from .models import *




from django.contrib.auth.models import User, Group
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import UserCreationForm, PasswordChangeForm
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse, Http404
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt
from django.contrib import messages
import employee
import academic
from academic.models import *
from academic.models import ClassInfo
from administration.models import *
from student.forms import PasswordChange
import pandas as pd
from django.conf import settings
import uuid
import logging

import openpyxl
import xlwt


logger = logging.getLogger(__name__)

# Create your views here.




def export_teachers_registration_xls(request):
	response = HttpResponse(content_type='application/ms-excel')
	response['Content-Disposition'] = 'attachment; filename=Teachers_Registration_Excel_Format.xls'

	wb = xlwt.Workbook(encoding='utf-8')
	ws = wb.add_sheet('Sheet1')

	row_num = 0

	font_style = xlwt.XFStyle()
	font_style.font.bold = True

	columns = ["Username*", "First Name*", "Last Name*", "Email*", "Password*", "Date Employed", "D.O.B", "Gender", "Marital Status", "Phone No.", "Address", "Religion", "Nationality"]


	for col_num in range(len(columns)):
		ws.write(row_num, col_num, columns[col_num], font_style)


	font_style = xlwt.XFStyle()

	wb.save(response)
	return response




@login_required(login_url='administration:login')
def load_upazilla(request):
    district_id = request.GET.get('district')
    upazilla = Upazilla.objects.filter(district_id=district_id).order_by('name')

    upazilla_id = request.GET.get('upazilla')
    union = Union.objects.filter(upazilla_id=upazilla_id).order_by('name')
    context = {
        'upazilla': upazilla,
        'union': union
    }
    return render(request, 'others/upazilla_dropdown_list_options.html', context)




@login_required(login_url='administration:login')
@permission_required('administration.delete_result')
def admin_edit(request, username):
    if request.method == 'POST' and request.is_ajax():
        print("it got here")
        user = User.objects.get(username=username)
        try:
            profi_info = PersonalInfo.objects.get(user=user)
        except:
            profi_info = None
        user_creation_form = UserCreationForm(instance=user)
        if profi_info != None:
            form = AdminEditForm(instance=profi_info)
        else:
            form = AdminEditForm()
        print("it got here2")
        context = {
            'form': form,
            'user_creation_form': user_creation_form,
        }
        html = render_to_string('teacher/admin-info-edit.html', context, request=request)
        return JsonResponse({'form': html})

    if request.method == 'POST':
        user = User.objects.get(username=username)
        #profi_info = PersonalInfo.objects.get(user=user)
        try:
            profi_info = PersonalInfo.objects.get(user=user)
        except:
            profi_info = None
        if profi_info != None:
            form = PersonalInfoForm(instance=profi_info, data=request.POST, files=request.FILES)
        else:
            form = PersonalInfoForm(data=request.POST, files=request.FILES)


        if form.is_valid():
            personal_info = form.save(commit=False)
            personal_info.user = user
            personal_info.save()
            messages.success(request, "Admin edited successfuly", extra_tags='success')
        else:
            messages.success(request, "Admin not edited", extra_tags='error')
    return redirect('home')




@login_required(login_url='administration:login')
@permission_required('administration.add_result')
def teacher_registration(request):
    user_creation_form = UserCreationForm()
    class_info = ClassInfoForm()
    education_form = EducationInfoForm()
    excell_upload_form = ExcelUploadForm()



    form = PersonalInfoForm()
    if request.method == 'POST':
        form = PersonalInfoForm(request.POST, request.FILES)
        user_creation_form = UserCreationForm(request.POST or None)
        education_form = EducationInfoForm(request.POST, request.FILES)

        if form.is_valid() and user_creation_form.is_valid() and education_form.is_valid():
            personal_info = form.save(commit=False)
            user = user_creation_form.save(commit=False)
            education = education_form.save(commit=False)
            personal_info.pass_word = request.POST['password1']
            user.first_name = personal_info.first_name
            user.last_name = personal_info.last_name
            user.save()
            education.user = user
            education.save()
            personal_info.user = user
            personal_info.education = education
            personal_info.save()
            personal_info = PersonalInfo.objects.get(user=user)
            idd = str(personal_info.id)
            if len(idd)  == 1:
                code = ("0" * 2) + idd + "/" + str(personal_info.date_employed)[2:4]
            elif len(idd) == 2:
                code = "0" + idd + "/" + str(personal_info.date_employed)[2:4]
            else:
                code = idd + "/" + str(personal_info.date_employed)[2:4]
            personal_info.employee_code = code
            personal_info.save()
            my_group = Group.objects.get(name='Teachers')
            user = User.objects.get(username=user.username)
            my_group.user_set.add(user)
            messages.success(request, "Teacher created successfuly", extra_tags='success')
            return redirect('teacher-registration')

    context = {
        'form': form,
        'class_info': class_info,
        'user_creation_form': user_creation_form,
        'education_form': education_form,
        'excell_upload_form': excell_upload_form
    }
    return render(request, 'teacher/teacher-registration.html', context)







@login_required(login_url='administration:login')
@permission_required('administration.add_result')
def excell_teacher_registration(request):
    if request.method == 'POST':

        new_teachers = request.FILES['xlsx']
        print("new_teachers")
        print(new_teachers)

        if not new_teachers.name.endswith('xlsx'):
            messages.success(request, "Wrong Format", extra_tags='error')
            return redirect('teacher-registration')
        else:
            exceled_upload_obj = ExcelUpload.objects.create(excell_sheet=request.FILES['xlsx'])
            df_open = openpyxl.load_workbook(f"{settings.BASE_DIR}/static/media/{exceled_upload_obj.excell_sheet}", read_only=True)
            first_sheet = df_open.get_sheet_names()[0]
            ws = df_open.get_sheet_by_name(first_sheet)
            for teacher in ws.iter_rows(row_offset=1):
                new_teacher = User(
                    username = teacher[0].value,
                    first_name = teacher[1].value,
                    last_name = teacher[2].value,
                    email = teacher[3].value
                    )
                new_teacher.save()
                new_teacher.set_password(teacher[4].value)
                new_teacher.save()
                user = User.objects.get(username=teacher[0].value)
                my_group = Group.objects.get(name='Teachers')
                my_group.user_set.add(user)
                new_teacher_personal_info = PersonalInfo(
                    first_name = teacher[1].value,
                    last_name = teacher[2].value,
                    pass_word = teacher[4].value,
                    user = user,
                    date_employed = teacher[5].value,
                    date_of_birth = teacher[6].value,
                    gender = teacher[7].value,
                    address = teacher[10].value,
                    religion = teacher[11].value,
                    nationality = teacher[12].value
                    )
                new_teacher_personal_info.save()
                idd = str(new_teacher_personal_info.id)
                if len(idd)  == 1:
                    code = ("0" * 2) + idd + "/" + str(new_teacher_personal_info.date_employed)[2:4]
                elif len(idd) == 2:
                    code = "0" + idd + "/" + str(new_teacher_personal_info.date_employed)[2:4]
                else:
                    code = idd + "/" + str(new_teacher_personal_info.date_employed)[2:4]
                new_teacher_personal_info.employee_code = code
                new_teacher_personal_info.save()
            exceled_upload_obj.delete()
        messages.success(request, "Teacher created successfuly", extra_tags='success')
        return redirect('teacher-registration')




@login_required(login_url='administration:login')
@permission_required('teacher.delete_personal_info')
def make_admin(request, username):
    user = User.objects.get(username=username)
    grab_teacher_group = Group.objects.get(name='Teachers')
    print(grab_teacher_group)
    print("still in teacher group")
    grab_teacher_group.user_set.remove(user)
    print(grab_teacher_group)
    print("removed from teacher group")
    grab_Admin2_group = Group.objects.get(name='Admin2')
    print(grab_Admin2_group)
    print("Admin2 group")
    grab_Admin2_group.user_set.add(user)
    print(grab_Admin2_group)
    print("added to Admin2 group")

    return redirect('teacher-list')





@login_required(login_url='administration:login')
@permission_required('teacher.delete_personal_info')
def remove_admin(request, username):
    user = User.objects.get(username=username)
    grab_Admin2_group = Group.objects.get(name='Admin2')
    print(grab_Admin2_group)
    print("Admin2 group")
    grab_Admin2_group.user_set.remove(user)
    print(grab_Admin2_group)
    print("removed from Admin2 group")
    grab_teacher_group = Group.objects.get(name='Teachers')
    print(grab_teacher_group)
    print("teacher group")
    grab_teacher_group.user_set.add(user)
    print(grab_teacher_group)
    print("added to teacher group")
    return redirect('teacher-list')




@login_required(login_url='administration:login')
@permission_required('administration.delete_result')
def teacher_list(request):
    if request.method == 'POST' and request.is_ajax():
        forms2 = ClassAssignForm()
        context = {
            'forms2': forms2
        }
        html = render_to_string('teacher/class-assign.html', context, request=request)
        return JsonResponse({'form': html})

    teachers = PersonalInfo.objects.filter(is_delete=False)
    if len(teachers) > 0:

        paginator = Paginator(teachers, 10)
        page = request.GET.get('page')
        try:
            teachers = paginator.page(page)
        except PageNotAnInteger:
            teachers = paginator.page(1)
        except EmptyPage:
            teachers = paginator.page(paginator.num_pages)
    else:
        teachers = None
    context = {
        'teacher': teachers
        # 'img':response
    }


    return render(request, 'teacher/teacher-list.html', context)




@login_required(login_url='administration:login')
@permission_required('administration.delete_result')
def teacher_list_search(request):
    if request.method == 'POST' and request.is_ajax():
        news_search = request.POST['search']
        if news_search:
            teachers = PersonalInfo.objects.filter(
                Q(first_name__icontains=news_search)|
                Q(employee_code__icontains=news_search)|
                Q(user__username__icontains=news_search)|
                Q(classes_assigned__icontains=news_search)|
                Q(last_name__icontains=news_search)).filter(is_delete=False).order_by('-id')

        if len(teachers) > 0:

            paginator = Paginator(teachers, 10)
            page = request.GET.get('page')
            try:
                teachers = paginator.page(page)
            except PageNotAnInteger:
                teachers = paginator.page(1)
            except EmptyPage:
                teachers = paginator.page(paginator.num_pages)
        else:
            teachers = None
        context = {
            'teacher': teachers
        }
        html = render_to_string('teacher/teacher-list-search.html', context, request=request)
        return JsonResponse({'form': html})
    teachers = PersonalInfo.objects.filter(is_delete=False)
    context = {
        'teacher': teachers
        # 'img':response
    }


    return render(request, 'teacher/teacher-list.html', context)







@login_required(login_url='administration:login')
@permission_required('administration.delete_result')
def teacher_assign_delete(request, username):
    if request.method == 'POST' and request.is_ajax():
        user = User.objects.get(username=username)
        forms2 = ClassAssignDeleteForm(user)
        context = {
            'forms2': forms2
        }
        html = render_to_string('teacher/class-assign.html', context, request=request)
        return JsonResponse({'form': html})

    if request.method == 'POST':
        assigned = []
        class_assigned = request.POST.getlist('class_info')
        for h in class_assigned:
            if h != '':
                if h not in assigned:
                    assigned.append(h)

        user = User.objects.get(username=username)
        personal_info = PersonalInfo.objects.get(user=user)
        for i in assigned:
            idd = int(i)
            class_obj = ClassInfo.objects.get(id=idd).name
            print(class_obj)
            try:
                check = ClassAssignedToTeacher.objects.get(name=class_obj, user=user, personal_info=personal_info)
            except:
                check = None
            if check != None:
                check.delete()

        try:
            trial = ClassAssignedToTeacher.objects.filter(user=user)
        except:
            trial = None
        assignedd = []
        if trial != None:
            for h in trial:
                if h != '':
                    if h not in assignedd:
                        assignedd.append(h)
            personal_info.classes_assigned = assignedd
            personal_info.save()
    return redirect('teacher-list')




@login_required(login_url='administration:login')
@permission_required('administration.add_result')
def change_password(request, username=None):
    user = User.objects.get(username=username)
    profi_info = PersonalInfo.objects.get(user=user)
    full_name = profi_info.first_name.capitalize() + ' ' + profi_info.last_name.capitalize()

    if request.method == 'POST' and request.is_ajax():
        personal_info = PasswordChange(instance=profi_info)
        form =PasswordChangeForm(user)


        context = {
            'form': form,
            'personal_info': personal_info,
            'full_name': full_name
        }
        html = render_to_string('student/password-change.html', context, request=request)
        return JsonResponse({'form': html})

    if request.method == 'POST':
        form = PasswordChangeForm(user, request.POST)
        if form.is_valid():
            profi_info.pass_word = request.POST['new_password1']
            user = form.save()
            profi_info.save()
            update_session_auth_hash(request, user)
            messages.success(request, "Password Changed Successfuly", extra_tags='success')
            return redirect('teacher-list')
        else:
            messages.success(request, "Error, ensure both password are the same.", extra_tags='error')
            return redirect('teacher-list')



@login_required(login_url='administration:login')
@permission_required('administration.add_result')
def teacher_edit(request, username):
    if request.method == 'POST' and request.is_ajax():
        user = User.objects.get(username=username)
        profi_info = PersonalInfo.objects.get(user=user)
        user_creation_form = UserCreationForm(instance=user)
        form = TeacherEditForm(instance=profi_info)
        education_form = EducationInfoEditForm()
        context = {
            'form': form,
            'user_creation_form': user_creation_form,
            'education_form': education_form
        }
        html = render_to_string('teacher/teacher-info-edit.html', context, request=request)
        return JsonResponse({'form': html})

    if request.method == 'POST':
        user = User.objects.get(username=username)
        profi_info = PersonalInfo.objects.get(user=user)
        education_info = EducationInfo.objects.get(user=user)
        form = PersonalInfoForm(instance=profi_info, data=request.POST, files=request.FILES)
        education_form = EducationInfoEditForm(instance=education_info, data=request.POST, files=request.FILES)

        if form.is_valid() and education_form.is_valid():
            user = User.objects.get(username=username)
            personal_info = form.save(commit=False)
            education = education_form.save(commit=False)
            education.user = user
            education.save()
            personal_info.user = user
            personal_info.education = education
            personal_info.save()
            messages.success(request, "Teacher edited successfuly", extra_tags='success')
    return redirect('teacher-list')


@login_required(login_url='administration:login')
@permission_required('administration.delete_result')
def teacher_assign(request, username):
    if request.method == 'POST' and request.is_ajax():
        user = User.objects.get(username=username)
        forms2 = TeacherAssignForm(user)
        context = {
            'forms2': forms2
        }
        html = render_to_string('teacher/class-assign.html', context, request=request)
        return JsonResponse({'form': html})
    return redirect('teacher-list')


@login_required(login_url='administration:login')
@permission_required('administration.delete_result')
def teacher_assign1(request, username):
    if request.method == 'POST':
        print("here here")
        assigned = []
        class_assigned = request.POST.getlist('class_info')
        print("here class assign")
        print(class_assigned)
        for h in class_assigned:
            if h != '':
                if h not in assigned:
                    assigned.append(h)
        print("here here assign")
        print(assigned)
        user = User.objects.get(username=username)
        personal_info = PersonalInfo.objects.get(user=user)
        print("here here here")
        for i in assigned:
            idd = int(i)
            print(i)
            class_obj = ClassInfo.objects.get(id=idd).name
            print(class_obj)
            print("here here here here")
            try:
                check = ClassAssignedToTeacher.objects.get(name=class_obj, user=user, personal_info=personal_info)
            except:
                check = None
            if check == None:
                print("check")
                print(check)
                create_class_assign = ClassAssignedToTeacher(name=class_obj, user=user, personal_info=personal_info)
                create_class_assign.save()
                # personal_info.classes_assigned = assigned
                # personal_info.save()
        try:
            trial = ClassAssignedToTeacher.objects.filter(user=user)
        except:
            trial = None
        assignedd = []
        if trial != None:
            for h in trial:
                if h != '':
                    if h not in assignedd:
                        assignedd.append(h)
            personal_info.classes_assigned = assignedd
            personal_info.save()
        return redirect('teacher-list')


@login_required(login_url='administration:login')
@permission_required('administration.add_result')
def teacher_profile(request, teacher_id):
    teacher = PersonalInfo.objects.get(id=teacher_id)
    context = {
        'teacher': teacher
    }
    return render(request, 'teacher/teacher-profile.html', context)



@login_required(login_url='administration:login')
@permission_required('administration.delete_result')
def teacher_delete(request, username):
    user = User.objects.get(username=username)
    teacher = PersonalInfo.objects.get(user=user)
    teacher.is_delete = True
    if user.groups.filter(name='Admin2'):
        my_group = Group.objects.get(name='Admin2')
        my_group.user_set.remove(user)
    elif user.groups.filter(name='Teachers'):
        my_group = Group.objects.get(name='Teachers')
        my_group.user_set.remove(user)
    teacher.save()
    messages.success(request, "Teacher Deleted Temporarily", extra_tags='success')
    return redirect('teacher-list')




@login_required(login_url='administration:login')
@permission_required('administration.delete_result')
def deleted_teachers(request, username=None):
    if username != None:
        try:
            user = User.objects.get(username=username)
        except:
            user = None

        if user == None:
            messages.success(request, "rRefresh page and try deleting again", extra_tags='error')
            return redirect('deleted-teachers')
        else:
            print(user)
            user.delete()
            messages.success(request, "Teacher Deleted Completely", extra_tags='success')
            return redirect('deleted-teachers')

    else:
        teachers = PersonalInfo.objects.filter(is_delete=True)
        if len(teachers) > 0:

            paginator = Paginator(teachers, 10)
            page = request.GET.get('page')
            try:
                teachers = paginator.page(page)
            except PageNotAnInteger:
                teachers = paginator.page(1)
            except EmptyPage:
                teachers = paginator.page(paginator.num_pages)
        else:
            teachers = None



    context = {
        'teacher': teachers
        # 'img':response
    }
    return render(request, 'teacher/deleted-teacher-list.html', context)


@login_required(login_url='administration:login')
@permission_required('administration.delete_result')
def restore_teachers_re(request, username):
    user = User.objects.get(username=username)
    teacher = PersonalInfo.objects.get(user=user)
    teacher.is_delete = False
    my_group = Group.objects.get(name='Teachers')
    my_group.user_set.add(user)
    teacher.save()
    messages.success(request, "Teacher Restored Successfuly", extra_tags='success')
    return redirect('deleted-teachers')



@login_required(login_url='administration:login')
@permission_required('administration.delete_result')
def create_class(request):
    if request.method == 'POST' and request.is_ajax():
        class_name = request.POST['class_name']
        class_class = ClassInfo.objects.get(name=class_name)
        forms2 = ClassInfoForm(instance=class_class)
        context = {
            'forms2': forms2,
            'class_name': class_name
        }
        html = render_to_string('teacher/edit-class.html', context, request=request)
        return JsonResponse({'form': html})
    if request.method == 'POST':
        forms1 = ClassInfoForm(request.POST or None)
        print(forms1)
        if forms1.is_valid():

            form = forms1.save(commit=False)
            form.name = form.name.upper()
            print(form.name)

            form.display_name = form.name.upper()
            try:
                check = ClassInfo.objects.get(name=form.name)
                print("its not supposed")
            except:
                check = None
                print("its not")
            if check == None:
                form.save()
                forms1 = ClassInfoForm()
                all_class = ClassInfo.objects.all().order_by('-id')


                context = {
                    'forms1': forms1,
                    'all_class': all_class,

                }

                messages.success(request, "Class created successfuly", extra_tags='success')
                return render(request, 'teacher/create-class2.html', context)
            else:
                all_class = ClassInfo.objects.all().order_by('-id')
                forms1 = ClassInfoForm()


                context = {
                    'forms1': forms1,
                    'all_class': all_class

                }
                messages.success(request, "Class already exist", extra_tags='error')
                return render(request, 'teacher/create-class2.html', context )
        else:
            all_class = ClassInfo.objects.all().order_by('-id')
            forms1 = ClassInfoForm()


            context = {
                'forms1': forms1,
                'all_class': all_class

            }
            messages.success(request, "Invalid form submission", extra_tags='error')
            return render(request, 'teacher/create-class2.html', context )
    else:
        forms1 = ClassInfoForm()


    all_class = ClassInfo.objects.all().order_by('-id')


    context = {
        'forms1': forms1,
        'all_class': all_class

    }


    return render(request, 'teacher/create-class2.html', context)



@login_required(login_url='administration:login')
@permission_required('administration.delete_result')
def create_class_edit(request):
    if request.method == 'POST':
        new_name = request.POST['name']
        prev_name = request.POST['prev_name']
        rem = prev_name.strip()
        print(rem)
        class_obj = ClassInfo.objects.get(name=prev_name)
        class_obj.name = new_name.upper()
        class_obj.display_name = new_name.upper()
        class_obj.save()
        return redirect('create-classroom')
    else:
        return redirect('create-classroom')


@login_required(login_url='administration:login')
@permission_required('administration.delete_result')
def create_class_delete(request, class_name):
    class_obj = ClassInfo.objects.get(name=class_name)
    class_obj.delete()
    return redirect('create-classroom')

