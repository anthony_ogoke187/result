from django.contrib import admin
from . import models



admin.site.register(models.EducationInfo)
admin.site.register(models.PersonalInfo)
admin.site.register(models.ClassAssignedToTeacher)
admin.site.register(models.ExcelUpload)
