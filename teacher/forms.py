from django import forms
from teacher.models import ClassAssignedToTeacher
from .models import *
from academic.models import *
from student.models import *
from administration.models import *
from .models import PersonalInfo, ExcelUpload
from django.core.exceptions import ValidationError


class ClassAssignForm(forms.ModelForm):
    class Meta:
        model = AcademicInfo
        fields = [
            'class_info',
            ]


class AdminEditForm(forms.ModelForm):
    class Meta:
        model = PersonalInfo
        exclude = {'date_employed', 'employee_code', 'driving_license_passport', 'classes_assigned', 'education', 'user'}
        widgets = {
            'first_name': forms.TextInput(attrs={'class': 'form-control lab'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control lab'}),
            'photo': forms.ClearableFileInput(attrs={'class': 'form-control lab'}),
            'date_of_birth': forms.DateInput(attrs={'class': 'form-control lab'}),
            'nationality': forms.Select(attrs={'class': 'form-control lab'}),
            'religion': forms.Select(attrs={'class': 'form-control lab'}),
            'gender': forms.Select(attrs={'class': 'form-control lab'}),
            'address': forms.Textarea(attrs={'class': 'form-control lab'}),
            'phone_no': forms.NumberInput(attrs={'class': 'form-control lab'}),
            'marital_status': forms.Select(attrs={'class': 'form-control lab'}),

        }



class EducationInfoForm(forms.ModelForm):
    class Meta:
        model = EducationInfo
        exclude = {'user'}
        widgets = {
            'institute': forms.TextInput(attrs={'class': 'form-control'}),
            'name_of_degree': forms.TextInput(attrs={'class': 'form-control'}),
            'trainer': forms.TextInput(attrs={'class': 'form-control'}),
            'passing_year_date': forms.DateInput(attrs={'class': 'form-control'}),
            'grade': forms.NumberInput(attrs={'class': 'form-control', 'step': '0.01'}),
            'certificate': forms.ClearableFileInput(attrs={'class': 'form-control'}),
            'upload_cv': forms.ClearableFileInput(attrs={'class': 'form-control'}),
        }

    def clean_certificate(self):
        try:
            data = self.cleaned_data['certificate']
        except:
            data = None

        #check if file size is what we expect
        if data != None:
            file_size = int(data.size)
            if file_size <= 1048576:
                #check if the content type is what we expect
                content_type = data.content_type
                if content_type == 'application/pdf':
                    return data
                else:
                    raise ValidationError('Invalid file type, you can only upload pdf documents')
            else:
                raise ValidationError('Invalid file size, you can only upload files of about 1mb')
        else:
            return data

    def clean_upload_cv(self):

        try:
            data = self.cleaned_data['upload_cv']
        except:
            data = None


        #check if file size is what we expect
        if data != None:
            file_size = int(data.size)
            if file_size <= 1048576:
                #check if the content type is what we expect
                content_type = data.content_type
                if content_type == 'application/pdf' or 'application/doc' or 'application/docx':
                    return data
                else:
                    raise ValidationError('Invalid file type, you can only upload pdf and microsoft-word documents')
            else:
                raise ValidationError('Invalid file size, you can only upload files of about 1mb')
        else:
            return data




class TeacherAssignForm(forms.ModelForm):
    class Meta:
        model = AcademicInfo
        fields = [
            'class_info',
            ]








class TeacherAssignForm(forms.ModelForm):
    def __init__(self, user, *args, **kwargs):
        super(TeacherAssignForm, self).__init__(*args, **kwargs)

        try:
            class_assigned = ClassAssignedToTeacher.objects.filter(user=user)
            print(class_assigned)
        except:
            class_assigned = None
            print(class_assigned)

        if class_assigned != None:
            try:
                teacher_personal_info = self.fields['class_info'].queryset.exclude(name__in=[p.name for p in class_assigned])
            except:
                teacher_personal_info = self.fields['class_info'].queryset
        else:
            teacher_personal_info = self.fields['class_info'].queryset


        self.fields['class_info'].queryset = teacher_personal_info

    class Meta:
        model = AcademicInfo
        fields = [
            'class_info',
            ]







class ClassAssignDeleteForm(forms.ModelForm):
    def __init__(self, user, *args, **kwargs):
        super(ClassAssignDeleteForm, self).__init__(*args, **kwargs)

        try:
            class_assigned = ClassAssignedToTeacher.objects.filter(user=user)
            print(class_assigned)
        except:
            class_assigned = None
            print(class_assigned)

        if class_assigned != None:
            try:
                teacher_personal_info = self.fields['class_info'].queryset.filter(name__in=[p.name for p in class_assigned])
            except:
                teacher_personal_info = self.fields['class_info'].queryset
        else:
            teacher_personal_info = self.fields['class_info'].queryset


        self.fields['class_info'].queryset = teacher_personal_info

    class Meta:
        model = AcademicInfo
        fields = [
            'class_info',
            ]



# class ClassAssignEditForm(forms.ModelForm):
#     class Meta:
#         model = AcademicInfo
#         fields = [
#             'class_info',
#             ]



class ClassInfoForm(forms.ModelForm):
    class Meta:
        model = ClassInfo
        fields = (
            'name',
            )




class EducationInfoEditForm(forms.ModelForm):
    class Meta:
        model = EducationInfo
        exclude = {'user'}
        widgets = {
            'institute': forms.TextInput(attrs={'class': 'form-control lab'}),
            'name_of_degree': forms.TextInput(attrs={'class': 'form-control lab'}),
            'trainer': forms.TextInput(attrs={'class': 'form-control lab'}),
            'passing_year_date': forms.DateInput(attrs={'class': 'form-control lab'}),
            'grade': forms.NumberInput(attrs={'class': 'form-control lab'}),
            'certificate': forms.ClearableFileInput(attrs={'class': 'form-control lab'}),
            'upload_cv': forms.ClearableFileInput(attrs={'class': 'form-control lab'}),
        }


    def clean_certificate(self):
        try:
            data = self.cleaned_data['certificate']
        except:
            data = None


        #check if file size is what we expect
        if data != None:
            file_size = int(data.size)
            if file_size <= 1048576:
                #check if the content type is what we expect
                content_type = data.content_type
                if content_type == 'application/pdf':
                    return data
                else:
                    raise ValidationError('Invalid file type, you can only upload pdf documents')
            else:
                raise ValidationError('Invalid file size, you can only upload files of about 1mb')
        else:
            return data


    def clean_upload_cv(self):
        try:
            data = self.cleaned_data['upload_cv']
        except:
            data = None


        #check if file size is what we expect
        if data != None:
            file_size = int(data.size)
            if file_size <= 1048576:
                #check if the content type is what we expect
                content_type = data.content_type
                if content_type == 'application/pdf' or 'application/doc' or 'application/docx':
                    return data
                else:
                    raise ValidationError('Invalid file type, you can only upload pdf and microsoft-word documents')
            else:
                raise ValidationError('Invalid file size, you can only upload files of about 1mb')
        else:
            return data




class TeacherEditForm(forms.ModelForm):
    class Meta:
        model = PersonalInfo
        exclude = {'date_employed', 'employee_code', 'driving_license_passport', 'classes_assigned', 'education', 'user'}
        widgets = {
            'first_name': forms.TextInput(attrs={'class': 'form-control lab'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control lab'}),
            'photo': forms.ClearableFileInput(attrs={'class': 'form-control lab'}),
            'date_of_birth': forms.DateInput(attrs={'class': 'form-control lab'}),
            'nationality': forms.Select(attrs={'class': 'form-control lab'}),
            'religion': forms.Select(attrs={'class': 'form-control lab'}),
            'gender': forms.Select(attrs={'class': 'form-control lab'}),
            'address': forms.Textarea(attrs={'class': 'form-control lab'}),
            'phone_no': forms.NumberInput(attrs={'class': 'form-control lab'}),
            'marital_status': forms.Select(attrs={'class': 'form-control lab'}),

        }



class PersonalInfoForm(forms.ModelForm):
    class Meta:
        model = PersonalInfo
        exclude = {'employee_code', 'driving_license_passport', 'classes_assigned', 'education', 'user', 'is_delete'}
        widgets = {
            'first_name': forms.TextInput(attrs={'class': 'form-control'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control'}),
            'email': forms.EmailInput(attrs={'class': 'form-control'}),
            'photo': forms.ClearableFileInput(attrs={'class': 'form-control'}),
            'date_employed': forms.DateInput(attrs={'class': 'form-control'}),
            'date_of_birth': forms.DateInput(attrs={'class': 'form-control'}),
            'nationality': forms.Select(attrs={'class': 'form-control'}),
            'religion': forms.Select(attrs={'class': 'form-control'}),
            'gender': forms.Select(attrs={'class': 'form-control'}),
            'address': forms.Textarea(attrs={'class': 'form-control'}),
            'phone_no': forms.NumberInput(attrs={'class': 'form-control'}),
            'marital_status': forms.Select(attrs={'class': 'form-control'}),

        }


class ExcelUploadForm(forms.ModelForm):
    class Meta:
        model = ExcelUpload
        fields = {'excell_sheet'}
        widgets = {
            'excell_sheet': forms.ClearableFileInput(attrs={'class': 'form-control'}),

        }

