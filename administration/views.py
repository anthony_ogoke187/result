from django.contrib.auth.models import User, Group
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from django.urls import reverse
from django.shortcuts import render, get_object_or_404, redirect
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse, Http404
from django.conf import settings
from django.contrib import messages
from student.models import AcademicInfo
from student.models import PersonalInfo as StudentPersonalInfo
from.forms import *
from.models import *
from teacher.models import Department, Designation
from teacher.models import PersonalInfo as TeacherPersonalInfo
from django.contrib.auth.decorators import permission_required
#import xlwt
from django.contrib.auth.decorators import login_required




from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags

import logging


logger = logging.getLogger(__name__)



@permission_required('administration.add_pincode')
def pin_in_allow(request):
    forms = AllowPinForm()
    if request.method == 'POST':

        try:
            current_allow_mode = CurrentAllowMode.objects.all().order_by('id')
        except:
            current_allow_mode = None

        if current_allow_mode != None:
            for i in current_allow_mode:
                i.delete()
        else:
            pass
        pin_allow_mode = PinAllow.objects.get(pin_allow=request.POST['pin_allow'])
        new_mode = CurrentAllowMode(name=pin_allow_mode)
        new_mode.save()
        return redirect('administration:pin-allow')
    mode = PinAllow.objects.all().order_by('id')
    current_allow_mode = CurrentAllowMode.objects.all().order_by('id')
    context = {
        'forms': forms,
        'current_allow_mode': current_allow_mode,
        'mode': mode
    }
    return render(request, 'academic/allow-pin.html', context)




@permission_required('administration.add_pincode')
def pin_in_database(request):
    pins = PinCode.objects.all()
    return render(request, 'administration/pin-preview-database.html', {'pins': pins})



@permission_required('administration.add_pincode')
def pincode_generated(request):
    if request.method == 'POST':
        forms = NumberOfPin(request.POST)
        if forms.is_valid():
            num = request.POST['num']
            expiry_date = request.POST['expiry_date']
            number = int(num)
            paragraphs = []
            for i in range(number):
                r = uuid.uuid4()
                reference = str(r)[:8]
                pin = PinCode(reference=reference, expiry_date=expiry_date)
                pin.save()
                paragraphs.append(pin)
            pins = PinCode.objects.all()
            return render(request, 'administration/pin-preview.html', {'paragraphs': paragraphs, 'pins': pins})

            # html = render_to_string('administration/pdf_template.html', context, request=request)
            # text_content = strip_tags(html)
            # email = EmailMultiAlternatives(
            #     #subject

            #     "Pin Codes",
            #     #content
            #     text_content,
            #     #from email
            #     settings.EMAIL_HOST_USER,
            #     #rec list
            #     [settings.EMAIL_HOST_USER]
            #     #[settings.EMAIL_HOST_USER]
            #     #[instructor_email]
            #     )
            # email.attach_alternative(html, "text/html")
            # email.send()
    else:
        forms = NumberOfPin()
    return render(request, 'administration/pin.html', {'forms': forms})


def signup(request):
    if request.method == 'POST':
        logger.error("Test!!")
        print('its working now')

        form = UserCreationForm(request.POST)
        form1 = StudentsLevelForm(request.POST)
        if form.is_valid() and form1.is_valid():
            new_user = form.save(commit=False)
            new_class = form1.save(commit=False)
            email = form.cleaned_data['email']
            class_level = form1.cleaned_data['level']
            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']
            new_user.set_password(form.cleaned_data['password1'])
            new_user.save()
            user = User.objects.get(username=username)
            new_class.students.add(user)
            #new_class.save()
            #Performance.clas.add(self.request.user)
            print(user.username)

            user = authenticate(username=username, password=password)
            if user:
                if user.is_active:
                    login(request, user)
                    if 'next' in request.POST:
                        messages.add_message(request, messages.SUCCESS, 'Account created successfully.')
                        return HttpResponseRedirect(reverse(request.POST.get('next')))
                    else:
                        messages.add_message(request, messages.SUCCESS, 'Account created successfully.')
                        return HttpResponseRedirect(reverse('rscheck:home_view'))
                else:
                    return HttpResponse("User is not active")
            else:
                return HttpResponse("User is None")
    else:
        form = UserCreationForm()
        form1 = StudentsLevelForm()
    context = {
        'form': form,
        'form1': form1,
    }
    return render(request, "administration/register.html", context)




def admin_login(request):
    forms = AdminLoginForm()
    if request.method == 'POST':
        forms = UserLoginForm(request.POST)
        if forms.is_valid():
            username = forms.cleaned_data['username']
            password = forms.cleaned_data['password']
            user = authenticate(username=username, password=password)
            if user:
                user = User.objects.get(username=username)
                if user.groups.filter(name='Admin') or user.groups.filter(name='Teachers') or user.groups.filter(name='Students') or user.groups.filter(name='Admin1') or user.groups.filter(name='Admin2'):
                    login(request, user)
                    return redirect('home')
                else:
                    messages.success(request, "You do not have permission to log in", extra_tags='error')
                    return redirect('administration:login')
            else:
                messages.success(request, "You do not have permission to log in, please contact the school's administration.", extra_tags='error')
                return redirect('administration:login')


    context = {'forms': forms}
    return render(request, 'home-index.html', context)




def login_page(request):
    if request.method == 'POST':
        forms = UserLoginForm(request.POST)
        if forms.is_valid():
            username = forms.cleaned_data['username']
            password = forms.cleaned_data['password']
            pin = forms.cleaned_data['pin']
            # user = User.objects.get(username=username)
            try:
                user =  User.objects.get(username=username)
                username = user.username
                user = authenticate(username=username, password=password)
            except:
                user = None

            if user:
                if user.is_active:
                    try:
                        pin_available = PinCode.objects.get(reference=pin)
                    except ObjectDoesNotExist:
                        pin_available = None
                    if pin_available:
                        try:
                            pin_created = pin_available.created

                            check_count = PinCodeTime.objects.filter(reference=pin).count()
                            if check_count > 0:
                                check = PinCodeTime.objects.filter(reference=pin)
                                checking = PinCodeTime.objects.filter(reference=pin)[0]
                            else:
                                check = None
                        except ObjectDoesNotExist:
                            check = None
                        if check:
                            print(check)
                            if username == checking.user.username:
                                if check_count <= 4:
                                    if user.is_active:
                                        login(request, user)
                                        create_pin = PinCodeTime(reference=pin, user=user)
                                        create_pin.save()
                                        if 'next' in request.POST:
                                            messages.success(request, "login successful.", extra_tags='success')
                                            return redirect(request.POST.get('next'))
                                        else:
                                            messages.success(request, "login successful.", extra_tags='success')
                                            return HttpResponseRedirect(reverse('home'))
                                    else:
                                        messages.success(request, "User not active, register as a new user", extra_tags='error')
                                        return HttpResponseRedirect(reverse('administration:login'))
                                else:
                                    pin_available.delete()
                                    check = PinCodeTime.objects.filter(reference=pin)
                                    for i in check:
                                        i.delete()
                                    messages.success(request, "Pin use completed", extra_tags='error')
                                    return HttpResponseRedirect(reverse('administration:login'))
                            else:
                                messages.success(request, "Pin missmatch", extra_tags='error')
                                return HttpResponseRedirect(reverse('administration:login'))
                        else:
                            login(request, user)
                            create_pin = PinCodeTime(reference=pin, user=user)
                            create_pin.save()
                            if 'next' in request.POST:
                                messages.success(request, "login successful.", extra_tags='success')
                                return redirect(request.POST.get('next'))
                            else:
                                messages.success(request, "login successful.", extra_tags='success')
                                return HttpResponseRedirect(reverse('home'))
                    else:
                        messages.success(request, "wrong pin", extra_tags='error')
                        return HttpResponseRedirect(reverse('administration:login'))
                else:
                    messages.success(request, "Student not active", extra_tags='error')
                    return HttpResponseRedirect(reverse('administration:login'))
            else:
                messages.success(request, "Not a registered student", extra_tags='error')
                return HttpResponseRedirect(reverse('administration:login'))
        else:
            messages.success(request, "Invalid login details", extra_tags='error')
            return HttpResponseRedirect(reverse('administration:login'))
    else:
        forms = UserLoginForm()
    context = {
        'forms': forms,
        }
    return render(request, 'administration/login.html', context)

def admin_logout(request):
    logout(request)
    return redirect('home')


def add_designation(request):
    forms = AddDesignationForm()
    if request.method == 'POST':
        forms = AddDesignationForm(request.POST)
        if forms.is_valid():
            forms.save()
            return redirect('designation')
    designation = Designation.objects.all()
    context = {'forms': forms, 'designation': designation}
    return render(request, 'administration/designation.html', context)



