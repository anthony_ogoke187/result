from django.urls import path
from . import views

app_name = 'administration'

urlpatterns = [
    path('login', views.admin_login, name='login'),
    path('logout', views.admin_logout, name='logout'),
    path('designation', views.add_designation, name='designation'),
    path('pin/', views.pincode_generated, name='pincode_generated'),
    path('pin-database/', views.pin_in_database, name='pin-database'),
    path('pin-allow/', views.pin_in_allow, name='pin-allow'),
]
