
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
import uuid
from django.urls import reverse
from django.db.models.fields import BLANK_CHOICE_DASH
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from academic.models import *

from django.core.validators import FileExtensionValidator

from pdf2image import convert_from_path
from django.conf import settings
import os





COVER_PAGE_DIRECTORY = 'coverdirectory/'
PDF_DIRECTORY = 'pdfdirectory/'
COVER_PAGE_FORMAT = 'jpg'





class Designation(models.Model):
    name = models.CharField(max_length=100, unique=True)
    date = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.name



class PinCode(models.Model):

    reference           =       models.CharField(max_length=200, unique=True)
    created             =       models.DateTimeField(auto_now_add=True)
    updated             =       models.DateTimeField(auto_now=True)
    expiry_date             =       models.DateTimeField()

    class Meta:
    	ordering = ['-id']
    	verbose_name = 'Pin code'
    	verbose_name_plural = 'Pin codes'

    def __str__(self):
    	return self.reference


    def get_absolute_url(self):
    	return reverse("rscheck:first_page")



class ClassLevel(models.Model):

    class_level           =       models.CharField(blank=True, null=True, max_length=200)

    class Meta:
        ordering = ['-id']
        verbose_name = 'Class level'
        verbose_name_plural = 'Class level'

    def __str__(self):
        return self.class_level


    def get_absolute_url(self):
        return reverse("rscheck:first_page")





class Number(models.Model):

	NUM = (
        (1,'1'),
        (2,'2'),
        (3,'3'),
        (4,'4'),
        (5,'5'),
        (6,'6'),
        (8,'8'),
        (9,'9'),
        (10,'10'),
        (11,'11'),
        (12,'12'),
        (13,'13'),
        (14,'14'),
        (15,'15'),
        (16,'16'),
        (18,'18'),
        (19,'19'),
        (20,'20'),)

	numbers          =      models.CharField(blank=True, null=True, max_length=10, choices=BLANK_CHOICE_DASH + list(NUM))
	created             =       models.DateTimeField(auto_now_add=True)
	updated             =       models.DateTimeField(auto_now=True)
	expiry_date             =       models.DateTimeField()


	class Meta:
		ordering = ['-id']
		verbose_name = 'Number'
		verbose_name_plural = 'Numbers'



	def __str__(self):
		return self.numbers



	def get_absolute_url(self):
		return reverse("rscheck:first_page")




class BoolMode(models.Model):

    name          =      models.CharField(blank=True, null=True, max_length=20)
    date = models.DateField(auto_now_add=True, blank=True, null=True)


    class Meta:
        ordering = ['-id']
        verbose_name = 'Bool Mode'
        verbose_name_plural = 'Bool Mode'



    def __str__(self):
        return self.name



    def get_absolute_url(self):
        return reverse("rscheck:first_page")







class PinAllow(models.Model):
    NUM = (
        ('True','True'),
        ('False','False'),)

    pin_allow          =      models.CharField(blank=True, null=True, max_length=10, choices=BLANK_CHOICE_DASH + list(NUM))
    date = models.DateField(auto_now_add=True, blank=True, null=True)


    class Meta:
        ordering = ['-id']
        verbose_name = 'Pin Allow'
        verbose_name_plural = 'Pin Allow'



    def __str__(self):
        return self.pin_allow



    def get_absolute_url(self):
        return reverse("rscheck:first_page")





class Term(models.Model):

    term  =  models.CharField(max_length=20)
    date = models.DateField(auto_now_add=True, blank=True, null=True)


    class Meta:
        ordering = ['-id']
        verbose_name = 'Term'
        verbose_name_plural = 'Terms'



    def __str__(self):
        return self.term



    def get_absolute_url(self):
        return reverse("rscheck:first_page")




class Year(models.Model):

    time           =       models.CharField(max_length=200)
    date = models.DateField(auto_now_add=True, blank=True, null=True)

    class Meta:
        ordering = ['-id']
        verbose_name = 'Year'
        verbose_name_plural = 'year'

    def __str__(self):
        return self.time


    def get_absolute_url(self):
        return reverse("rscheck:first_page")






class StudentsInClass(models.Model):


    students = models.ManyToManyField(User, blank=True, related_name="class_joined")
    level    =   models.ForeignKey(ClassLevel, blank=True, null=True, on_delete=models.CASCADE, related_name="user_class")

    class Meta:
        ordering = ['-id']
        verbose_name = 'Students in class'
        verbose_name_plural = 'Students in class'



    def __str__(self):
        return self.students.username



    def get_absolute_url(self):
        return reverse("rscheck:first_page")


# this function is used to rename the pdf to the name specified by filename field
def set_pdf_file_name(instance):
    return os.path.join(PDF_DIRECTORY, '{}.pdf'.format(instance.pdf.name))



# not used in this example
def set_cover_file_name(instance):
    return os.path.join(COVER_PAGE_DIRECTORY, '{}.{}'.format(instance.pdf.name, COVER_PAGE_FORMAT))


class Result(models.Model):


    pdf = models.FileField(upload_to='pdfdirectory/', validators=[FileExtensionValidator(allowed_extensions=['pdf'])])
    coverpage  = models.ImageField(upload_to='coverdirectory/', null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user_performance")
    clas    =   models.ForeignKey(ClassInfo, blank=True, null=True, on_delete=models.CASCADE, related_name="user_class_level")
    term =  models.ForeignKey(Term, on_delete=models.CASCADE, related_name="user_term_result", null=True, blank=True)
    year =  models.ForeignKey(Year, on_delete=models.CASCADE, related_name="user_year_result", null=True, blank=True)


    class Meta:
        ordering = ['-id']
        verbose_name = 'Result'
        verbose_name_plural = 'Result'



    def __str__(self):
        return self.user.username



    def get_absolute_url(self):
        return reverse("rscheck:first_page")


def convert_pdf_to_image(sender, instance, created, **kwargs):
    if created:
        #check if COVER_PAGE_DIRECTORY exists, create it if it doesn't
        #have to do this because of setting coverpage attribute of instance programmatically
        cover_page_dir = os.path.join(settings.MEDIA_ROOT, COVER_PAGE_DIRECTORY)

        if not os.path.exists(cover_page_dir):
            os.mkdir(cover_page_dir)

        # convert page cover (in this case) to jpg and save

        cover_page_image = convert_from_path(
            pdf_path=instance.pdf.path,
            dpi=200,
            first_page=1,
            last_page=1,
            fmt=COVER_PAGE_FORMAT,
            output_folder=cover_page_dir,
        )[0]


        # get name of pdf_file
        pdf_filename, extension = os.path.splitext(os.path.basename(instance.pdf.name))
        new_cover_page_path = '{}.{}'.format(os.path.join(cover_page_dir, pdf_filename), COVER_PAGE_FORMAT)
        # rename the file that was saved to be the same as the pdf file
        os.rename(cover_page_image.filename, new_cover_page_path)
        # get the relative path to the cover page to store in model
        new_cover_page_path_relative = '{}.{}'.format(os.path.join(COVER_PAGE_DIRECTORY, pdf_filename), COVER_PAGE_FORMAT)
        instance.coverpage = new_cover_page_path_relative

        # call save on the model instance to update database record
        instance.save()

post_save.connect(convert_pdf_to_image, sender=Result)






# class StudentsBio(models.Model):


#     user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user_bio")
#     clas    =   models.ForeignKey(ClassLevel, blank=True, null=True, on_delete=models.CASCADE, related_name="user_class_bio")
#     term =  models.ForeignKey(Term, on_delete=models.CASCADE, related_name="user_term_result", null=True, blank=True)
#     year =  models.ForeignKey(Year, on_delete=models.CASCADE, related_name="user_year_result", null=True, blank=True)


#     class Meta:
#         ordering = ['-id']
#         verbose_name = 'Performance'
#         verbose_name_plural = 'Performance'



#     def __str__(self):
#         return self.user.username



#     def get_absolute_url(self):
#         return reverse("rscheck:first_page")


class PinCodeTime(models.Model):

    reference           =       models.CharField(max_length=200, editable=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user_result")
    result = 	models.ForeignKey(Result,  blank=True, null=True, on_delete=models.CASCADE, related_name="pin_connect_result")


    class Meta:
        ordering = ['-id']
        verbose_name = 'Pin Code Time'
        verbose_name_plural = 'Pin Codes Time'

    def __str__(self):
        return self.user.username


    def get_absolute_url(self):
        return reverse("rscheck:first_page")




class Administration(models.Model):


    term =  models.ForeignKey(Term, on_delete=models.CASCADE, related_name="new_term_result", null=True, blank=True)


    class Meta:
        ordering = ['-id']
        verbose_name = 'Administration'
        verbose_name_plural = 'Administration'



    def get_absolute_url(self):
        return reverse("rscheck:first_page")




class CurrentSession(models.Model):
	name = models.ForeignKey(Year, on_delete=models.CASCADE, related_name="admin_current_session")
	begining_date = models.DateField(auto_now_add=True, blank=True, null=True)
	end_date = models.DateTimeField(blank=True, null=True)







class CurrentTerm(models.Model):
    name = models.ForeignKey(Term, on_delete=models.CASCADE, related_name="admin_current_term")
    begining_date = models.DateField(auto_now_add=True, blank=True, null=True)
    end_date = models.DateTimeField(blank=True, null=True)


class CurrentAllowMode(models.Model):
    name = models.ForeignKey(PinAllow, on_delete=models.CASCADE, related_name="admin_current_allow_mode")
