from django import forms
from teacher import models
from .models import *





class AdminLoginForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Username'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Password'}))


# class AddDepartmentForm(forms.ModelForm):
#     class Meta:
#         model = models.Department
#         fields = '__all__'
#         widgets = {
#             'name': forms.TextInput(attrs={'class': 'form-control'}),
#         }
# class AddDesignationForm(forms.ModelForm):
#     class Meta:
#         model = models.Designation
#         fields = '__all__'
#         widgets = {
#             'name': forms.TextInput(attrs={'class': 'form-control'}),
#         }



class UserLoginForm(forms.Form):
    username = forms.CharField(label="", widget= forms.TextInput())
    password = forms.CharField(label="", widget=forms.PasswordInput())
    #pin = forms.CharField(label="", widget=forms.PasswordInput())


class NumberOfPin(forms.ModelForm):
    class Meta:
        model = Number
        fields = (
            'numbers',
            'expiry_date',
            )


class AllowPinForm(forms.ModelForm):
    class Meta:
        model = PinAllow
        fields = (
            'pin_allow',
            )

