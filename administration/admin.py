from django.contrib import admin
from .models import *

BoolMode

class PinCodeAdmin(admin.ModelAdmin):
    list_display = ('reference','created', 'updated', 'expiry_date',)

class PinCodeTimeAdmin(admin.ModelAdmin):
    list_display = ('reference', 'user', 'result',)


class CurrentAllowModeAdmin(admin.ModelAdmin):
    list_display = ('name',)


class BoolModeAdmin(admin.ModelAdmin):
    list_display = ('name',)

class PinAllowAdmin(admin.ModelAdmin):
    list_display = ('pin_allow',)



class ClassLevelAdmin(admin.ModelAdmin):
    list_display = ('class_level',)

class StudentsInClassAdmin(admin.ModelAdmin):
    list_display = ('level',)

class ResultAdmin(admin.ModelAdmin):
    list_display = ('user', 'term',)


class AdministrationAdmin(admin.ModelAdmin):
    list_display = ('term',)


class TermAdmin(admin.ModelAdmin):
    list_display = ('term',)


class YearAdmin(admin.ModelAdmin):
    list_display = ('time',)


class CurrentSessionAdmin(admin.ModelAdmin):
    list_display = ('name', 'begining_date', 'end_date',)


class CurrentTermAdmin(admin.ModelAdmin):
    list_display = ('name', 'begining_date', 'end_date',)

admin.site.register(PinCode, PinCodeAdmin)
admin.site.register(StudentsInClass, StudentsInClassAdmin)
admin.site.register(ClassLevel, ClassLevelAdmin)
admin.site.register(Result, ResultAdmin)
admin.site.register(PinCodeTime, PinCodeTimeAdmin)
admin.site.register(Term, TermAdmin)
admin.site.register(Year, YearAdmin)
admin.site.register(Administration, AdministrationAdmin)
admin.site.register(CurrentSession, CurrentSessionAdmin)
admin.site.register(CurrentTerm, CurrentTermAdmin)

admin.site.register(CurrentAllowMode, CurrentAllowModeAdmin)
admin.site.register(PinAllow, PinAllowAdmin)

admin.site.register(BoolMode, BoolModeAdmin)



