# Generated by Django 3.0.7 on 2022-03-28 10:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('student', '0018_auto_20220328_1022'),
    ]

    operations = [
        migrations.AlterField(
            model_name='excelupload',
            name='excell_sheet',
            field=models.FileField(null=True, upload_to=''),
        ),
    ]
