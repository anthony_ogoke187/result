from django import forms


from .models import *
#from teacher.models import *
from academic.models import ClassInfo
from administration.models import *

from student.models import PersonalInfo


from teacher.models import PersonalInfo as PInfo
from teacher.models import ClassAssignedToTeacher
from django.core.exceptions import ValidationError



class AcademicInfoForm(forms.ModelForm):
    def __init__(self, user, *args, **kwargs):
        super(AcademicInfoForm, self).__init__(*args, **kwargs)
        if user.groups.filter(name='Teachers'):
            try:
                class_assigned = ClassAssignedToTeacher.objects.filter(user=user)
            except:
                class_assigned = None

            if class_assigned != None:
                if user.is_authenticated:
                    try:
                        teacher_personal_info = self.fields['class_info'].queryset.filter(name__in=[p.name for p in class_assigned])
                    except:
                        teacher_personal_info = self.fields['class_info'].queryset
            else:
                teacher_personal_info = self.fields['class_info'].queryset


            self.fields['class_info'].queryset = teacher_personal_info
        elif user.groups.filter(name='Admin'):
            self.fields['class_info'].queryset = self.fields['class_info'].queryset

    class Meta:
        model = AcademicInfo
        fields = [
            'class_info',
            ]


class AcademicInfoEditForm(forms.ModelForm):
    class Meta:
        model = AcademicInfo
        fields = [
            'is_delete',
            ]

class PersonalInfoForm(forms.ModelForm):
    class Meta:
        model = PersonalInfo
        fields = '__all__'
        exclude = {'user', 'class_of_student', 'blood_group_choice', 'blood_group', 'birth_certificate_no'}
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'photo': forms.ClearableFileInput(attrs={'class': 'form-control'}),
            'blood_group': forms.Select(attrs={'class': 'form-control'}),
            'date_of_birth': forms.TextInput(attrs={'class': 'form-control'}),
            'gender': forms.Select(attrs={'class': 'form-control'}),
            'phone_no': forms.TextInput(attrs={'class': 'form-control'}),
            'email': forms.TextInput(attrs={'class': 'form-control'}),
            'birth_certificate_no': forms.TextInput(attrs={'class': 'form-control'}),
            'religion': forms.Select(attrs={'class': 'form-control'}),
            'nationality': forms.Select(attrs={'class': 'form-control'})
        }






class PersonalInfoFormEdit(forms.ModelForm):

    def __init__(self, user, *args, **kwargs):
        super(PersonalInfoFormEdit, self).__init__(*args, **kwargs)
        if user.groups.filter(name='Teachers'):
            try:
                class_assigned = ClassAssignedToTeacher.objects.filter(user=user)
            except:
                class_assigned = None

            if class_assigned != None:
                if user.is_authenticated:
                    try:
                        class_of_student = self.fields['class_of_student'].queryset.filter(name__in=[p.name for p in class_assigned])
                    except:
                        class_of_student = self.fields['class_of_student'].queryset
            else:
                class_of_student = self.fields['class_of_student'].queryset


            self.fields['class_of_student'].queryset = class_of_student
        elif user.groups.filter(name='Admin'):
            self.fields['class_of_student'].queryset = self.fields['class_of_student'].queryset


    class Meta:
        model = PersonalInfo
        fields = '__all__'
        exclude = {'user', 'blood_group_choice', 'blood_group', 'birth_certificate_no'}
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'photo': forms.ClearableFileInput(attrs={'class': 'form-control'}),
            'blood_group': forms.Select(attrs={'class': 'form-control'}),
            'date_of_birth': forms.TextInput(attrs={'class': 'form-control'}),
            'gender': forms.Select(attrs={'class': 'form-control'}),
            'phone_no': forms.TextInput(attrs={'class': 'form-control'}),
            'email': forms.TextInput(attrs={'class': 'form-control'}),
            'birth_certificate_no': forms.TextInput(attrs={'class': 'form-control'}),
            'religion': forms.Select(attrs={'class': 'form-control'}),
            'nationality': forms.Select(attrs={'class': 'form-control'})
        }






class PasswordChange(forms.ModelForm):

    class Meta:
        model = PersonalInfo
        fields = [
            'pass_word',
            ]
        widgets = {
            'PasswordChange': forms.TextInput(attrs={'class': 'form-control'})
        }








# class PersonalInfoFormEdit(forms.ModelForm):
#     class Meta:
#         model = PersonalInfo
#         fields = '__all__'
#         exclude = {'user', 'blood_group_choice', 'blood_group', 'birth_certificate_no'}
#         widgets = {
#             'name': forms.TextInput(attrs={'class': 'form-control'}),
#             'photo': forms.ClearableFileInput(attrs={'class': 'form-control'}),
#             'blood_group': forms.Select(attrs={'class': 'form-control'}),
#             'date_of_birth': forms.TextInput(attrs={'class': 'form-control'}),
#             'gender': forms.Select(attrs={'class': 'form-control'}),
#             'phone_no': forms.TextInput(attrs={'class': 'form-control'}),
#             'email': forms.TextInput(attrs={'class': 'form-control'}),
#             'birth_certificate_no': forms.TextInput(attrs={'class': 'form-control'}),
#             'religion': forms.Select(attrs={'class': 'form-control'}),
#             'nationality': forms.Select(attrs={'class': 'form-control'})
#         }





class StudentSearchForm1(forms.ModelForm):
    def __init__(self, user, *args, **kwargs):
        super(StudentSearchForm1, self).__init__(*args, **kwargs)
        if user.groups.filter(name='Teachers'):
            try:
                class_assigned = ClassAssignedToTeacher.objects.filter(user=user)
            except:
                class_assigned = None

            if class_assigned != None:
                if user.is_authenticated:
                    try:
                        teacher_personal_info = self.fields['class_info'].queryset.filter(name__in=[p.name for p in class_assigned])
                    except:
                        teacher_personal_info = self.fields['class_info'].queryset
            else:
                teacher_personal_info = self.fields['class_info'].queryset


            self.fields['class_info'].queryset = teacher_personal_info
        elif user.groups.filter(name='Admin'):
            self.fields['class_info'].queryset = self.fields['class_info'].queryset

    class Meta:
        model = AcademicInfo
        fields = [
            'class_info',
            ]



class StudentSearchForm2(forms.ModelForm):
    registration_no = forms.IntegerField(required=False, widget=forms.NumberInput(attrs={'placeholder': 'Registration No', 'aria-controls': 'DataTables_Table_0'}))

class EnrolledStudentForm(forms.Form):
    class_name = forms.ModelChoiceField(queryset=ClassInfo.objects.all())

class StudentEnrollForm(forms.Form):
    class_name = forms.ModelChoiceField(queryset=ClassRegistration.objects.all(), widget=forms.Select(attrs={'class': 'form-control'}))
    roll_no = forms.IntegerField(widget=forms.NumberInput(attrs={'placeholder': 'Enter Roll', 'class': 'form-control'}))

class SearchEnrolledStudentForm(forms.Form):
    reg_class = forms.ModelChoiceField(queryset=ClassRegistration.objects.all())
    roll_no = forms.IntegerField(required=False, widget=forms.NumberInput(attrs={'placeholder': 'Enter Roll'}))




class ResultForm(forms.ModelForm):

    class Meta:
        model = Result
        fields = [
            'pdf',
            ]


    def clean_pdf(self):
        try:
            data = self.cleaned_data['pdf']
        except:
            data = None

        #check if file size is what we expect
        if data != None:
            file_size = int(data.size)
            print(file_size)
            if file_size <= 1048576:
                #check if the content type is what we expect
                content_type = data.content_type
                if content_type == 'application/pdf':
                    return data
                else:
                    raise ValidationError('Invalid file type, you can only upload pdf documents')
            else:
                raise ValidationError('Invalid file size, you can only upload files of about 1mb')
        else:
            return data

class CertForm(forms.ModelForm):

    class Meta:
        model = Cert
        fields = [
            'pdf',
            ]







class ExcelUploadForm(forms.ModelForm):
    class Meta:
        model = ExcelUpload
        fields = {'excell_sheet'}
        widgets = {
            'excell_sheet': forms.ClearableFileInput(attrs={'class': 'form-control'}),

        }