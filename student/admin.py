from django.contrib import admin

from .models import *
# Register your models here.

admin.site.register(AcademicInfo)
admin.site.register(PersonalInfo)
admin.site.register(EnrolledStudent)
admin.site.register(ExcelUpload)
admin.site.register(Cert)
