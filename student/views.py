from django.shortcuts import render, redirect
from academic.models import ClassRegistration
from django.db.models import Count, Q
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from teacher.models import *
from .forms import *
from .models import *
import requests
import os


from django.contrib.auth.models import User, Group
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.forms import UserCreationForm
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse, Http404
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt
from django.contrib import messages
import teacher

import employee
import academic
from administration.models import *
from academic.models import ClassInfo
from pdf2image import convert_from_path, convert_from_bytes
import pandas as pd
from django.conf import settings
import uuid
import logging

import openpyxl
import xlwt

logger = logging.getLogger(__name__)




def export_students_registration_xls(request):
	response = HttpResponse(content_type='application/ms-excel')
	response['Content-Disposition'] = 'attachment; filename=Students_Excel_Registration_Format.xls'

	wb = xlwt.Workbook(encoding='utf-8')
	ws = wb.add_sheet('Sheet1')

	row_num = 0

	font_style = xlwt.XFStyle()
	font_style.font.bold = True

	columns = ["Class*", "Reg. No*", "First Name*", "Last Name*", "Email*", "Password*", "D.O.B", "Gender", "Phone No.", "Religion", "Nationality"]


	for col_num in range(len(columns)):
		ws.write(row_num, col_num, columns[col_num], font_style)


	font_style = xlwt.XFStyle()

	wb.save(response)
	return response


def load_upazilla(request):
    district_id = request.GET.get('district')
    upazilla = Upazilla.objects.filter(district_id=district_id).order_by('name')

    upazilla_id = request.GET.get('upazilla')
    union = Union.objects.filter(upazilla_id=upazilla_id).order_by('name')
    context = {
        'upazilla': upazilla,
        'union': union
    }
    return render(request, 'others/upazilla_dropdown_list_options.html', context)


def class_wise_student_registration(request):
    register_class = ClassRegistration.objects.all()
    context = {'register_class': register_class}
    return render(request, 'student/class-wise-student-registration.html', context)








@login_required(login_url='administration:login')
@permission_required('administration.add_result')
def student_registration(request):# receive name of Teacher from url which be sent from Teachers dashboard
    user = request.user

    try:
        teacher_info = PersonalInfo.objects.get(user=user)
    except:
        teacher_info = None

    if request.user.is_authenticated:
        if user.groups.filter(name='Admin'):
            teacher_personal_info = ClassInfo.objects.all()
        else:
            teacher_personal_info = []
            try:
                a = ClassAssignedToTeacher.objects.filter(user=user)
            except:
                a = None


            if a != None:
                for i in a:
                    if i.name not in teacher_personal_info:
                        teacher_personal_info.append(i.name)


    try:
        academic_info_form = AcademicInfoForm(user, request.POST or None)
    except:
        academic_info_form = None

    excell_upload_form = ExcelUploadForm()

    user_creation_form = UserCreationForm(request.POST or None)
    personal_info_form = PersonalInfoForm(request.POST or None, request.FILES or None)

    if request.method == 'POST':
        if academic_info_form != None:
            if academic_info_form.is_valid() and personal_info_form.is_valid() and user_creation_form.is_valid():
                academic_info = academic_info_form.save(commit=False)
                s7 = user_creation_form.save(commit=False)
                s1 = personal_info_form.save(commit=False)
                s1.pass_word = request.POST['password1']
                s7.first_name = s1.first_name
                s7.last_name = s1.last_name
                s7.save()
                s1.user = s7
                s1.class_of_student = academic_info.class_info
                s1.save()
                academic_info.registration_no = s7.username
                academic_info.personal_info = s1
                academic_info.save()
                my_group = Group.objects.get(name='Students')
                user = User.objects.get(username=s7.username)
                my_group.user_set.add(user)
                messages.success(request, "Student Added Successfuly", extra_tags='success')
                return redirect('student-registration')

    context = {
        'academic_info_form': academic_info_form,
        'user_creation_form': user_creation_form,
        'personal_info_form': personal_info_form,
        'teacher_personal_info': teacher_personal_info,
        'excell_upload_form': excell_upload_form
    }
    return render(request, 'student/student-registration22.html', context)



@login_required(login_url='administration:login')
@permission_required('administration.add_result')
def excell_student_registration(request):
    if request.method == 'POST':

        new_students = request.FILES['xlsx']
        print("new_students")
        print(new_students)

        if not new_students.name.endswith('xlsx'):
            messages.success(request, "Wrong Format", extra_tags='error')
            return redirect('student-registration')
        else:
            exceled_upload_obj = ExcelUpload.objects.create(excell_sheet=request.FILES['xlsx'])
            df_open = openpyxl.load_workbook(f"{settings.BASE_DIR}/static/media/{exceled_upload_obj.excell_sheet}", read_only=True)
            first_sheet = df_open.get_sheet_names()[0]
            ws = df_open.get_sheet_by_name(first_sheet)
            for student in ws.iter_rows(row_offset=1):
                new_student = User(
                    username = student[1].value,
                    first_name = student[2].value,
                    last_name = student[3].value,
                    email = student[4].value
                    )
                new_student.save()
                new_student.set_password(student[5].value)
                new_student.save()
                user = User.objects.get(username=student[1].value)
                class_info = ClassInfo.objects.get(name=student[0].value.upper())
                my_group = Group.objects.get(name='Students')
                my_group.user_set.add(user)
                print(class_info)
                new_student_personal_info = PersonalInfo(
                    class_of_student = class_info,
                    first_name = student[2].value,
                    last_name = student[3].value,
                    email = student[4].value,
                    pass_word = student[5].value,
                    user = user,
                    date_of_birth = student[6].value,
                    gender = student[7].value,
                    phone_no = student[8].value,
                    religion = student[9].value,
                    nationality = student[10].value
                    )
                new_student_personal_info.save()
                new_student_academic_info = AcademicInfo(
                    class_info = class_info,
                    registration_no = student[1].value,
                    personal_info = new_student_personal_info
                    )
                new_student_academic_info.save()
            exceled_upload_obj = ExcelUpload.objects.get(excell_sheet=exceled_upload_obj.excell_sheet)
            exceled_upload_obj.delete()
        messages.success(request, "Students Added Successfuly", extra_tags='success')
        return redirect('student-registration')



@csrf_exempt
@login_required(login_url='administration:login')
@permission_required('administration.add_result')
def student_upload_result(request, reg_no):
    if request.method == 'POST':
        forms1 = ResultForm(request.POST or None, request.FILES or None)
        if forms1.is_valid():
            form = forms1.save(commit=False)
            academic_info = AcademicInfo.objects.get(registration_no=reg_no)

            user = academic_info.personal_info.user
            clas = academic_info.class_info
            current_term1 = CurrentTerm.objects.all()[0].name
            current_term = Term.objects.get(term=current_term1)
            current_session1 = CurrentSession.objects.all()[0].name
            current_session = Year.objects.get(time=current_session1)
            try:
                perform = Result.objects.get(clas=clas, user=user, term=current_term, year=current_session)
            except:
                perform = None
            if perform == None:
                form.clas = clas
                form.user = user
                form.term = current_term
                form.year = current_session
                form.save()
                if academic_info:
                    academic_info.result = form
                    academic_info.save()
                """res = Result.objects.get(clas=clas, user=user, term=current_term, year=current_session)
                path_url = f'https://resultchecker.pythonanywhere.com{res.pdf.url}'
                pages = convert_from_path(pdf_path=res.pdf.path, dpi=500, single_file=True)
                page = pages[0]
                name111 = user.username
                sname = f'{name111}.jpg'
                page.save(sname, 'JPEG')
                res.img = page
                res.save()"""
                try:
                    student = AcademicInfo.objects.filter(class_info=clas).filter(is_delete=False).order_by('-id')
                except:
                    student = None

                if student != None:
                    paginator = Paginator(student, 10)
                    page = request.GET.get('page')
                    try:
                        student = paginator.page(page)
                    except PageNotAnInteger:
                        student = paginator.page(1)
                    except EmptyPage:
                        student = paginator.page(paginator.num_pages)

                forms = StudentSearchForm1(request.user)
                context = {
                    'student': student,
                    'forms': forms,
                    'class': clas,
                    'forms1': forms1,
                    'current_session1': current_session1,
                    'current_session': current_session,
                    'current_term1': current_term1,
                    'current_term': current_term,
                    # 'img':response
                }
                messages.success(request, "Result uploaded Successfuly", extra_tags='success')
                #return render(request, 'base/upload-res-return.html', context)
                return redirect('student-list-re', username=clas)
    else:
        forms1 = ResultForm()

    forms = StudentSearchForm1(request.user)
    academic_info = AcademicInfo.objects.get(registration_no=reg_no)
    clas_inf = academic_info.class_info
    clas = ClassInfo.objects.get(name=clas_inf)
    current_term1 = CurrentTerm.objects.all()[0].name
    current_term = Term.objects.get(term=current_term1)
    current_session1 = CurrentSession.objects.all()[0].name
    current_session = Year.objects.get(time=current_session1)
    student = AcademicInfo.objects.filter(class_info=clas_inf).filter(is_delete=False).order_by('-id')
    context = {
        'student': student,
        'forms': forms,
        'class': clas,
        'forms1': forms1,
        'current_session1': current_session1,
        'current_session': current_session,
        'current_term1': current_term1,
        'current_term': current_term,
        # 'img':response
    }
    messages.success(request, "Upload not Successful", extra_tags='error')
    #return render(request, 'base/upload-res-return.html', context)
    return redirect('student-list-re', username=clas)







@csrf_exempt
@login_required(login_url='administration:login')
@permission_required('administration.add_result')
def student_list(request, username=None):
    username = username
    forms = StudentSearchForm1(request.user)
    forms1 = ResultForm()

    student = AcademicInfo.objects.filter(is_delete=False).order_by('-id')

    try:
        current_term1 = CurrentTerm.objects.all()[0].name
        current_term = Term.objects.get(term=current_term1)
    except:
        current_term = None
        current_term1 = None


    try:
        current_session1 = CurrentSession.objects.all()[0].name
        current_session = Year.objects.get(time=current_session1)
    except:
        current_session = None
        current_session1 = None



    if request.method == 'POST':
        class_ = request.POST['class_info']
        class_info = ClassInfo.objects.get(name=class_).id

        student = AcademicInfo.objects.filter(class_info=class_info).filter(is_delete=False).order_by('-id')

        if len(student) > 0:

            paginator = Paginator(student, 10)
            page = request.GET.get('page')
            try:
                student = paginator.page(page)
            except PageNotAnInteger:
                student = paginator.page(1)
            except EmptyPage:
                student = paginator.page(paginator.num_pages)
        else:
            student = None
        context = {
            'student': student,
            'forms': forms,
            'class': class_,
            'forms1': forms1,
            'current_session': current_session,
            'current_term': current_term,
            'current_session1': current_session1,
            'current_term1': current_term1
            # 'img':response
        }
        #return render(request, 'student/student-list-django.html', context)
        if request.is_ajax():
            html = render_to_string('student/student-list_replace.html', context, request=request)
            return JsonResponse({'form': html})

        if username != None:
            pass

    context = {
        'student': student,
        'username': username,
        'forms': forms,
        'forms1': forms1,
        'current_session': current_session,
        'current_term': current_term
    }
    return render(request, 'student/student-list.html', context)






@csrf_exempt
@login_required(login_url='administration:login')
@permission_required('administration.add_result')
def student_list_search(request):
    group_students = []
    username = request.user
    user = User.objects.get(username=username)
    group_student = User.objects.filter(groups__name='Students')
    forms = StudentSearchForm1(request.user)
    forms1 = ResultForm()
    for i in group_student:
        group_students.append(i.username)
    try:
        result = Result.objects.filter(user=user)[0]
    except:
        result = None
    student = AcademicInfo.objects.filter(is_delete=False).order_by('-id')

    try:
        current_term1 = CurrentTerm.objects.all()[0].name
        current_term = Term.objects.get(term=current_term1)
    except:
        current_term = None
        current_term1 = None


    try:
        current_session1 = CurrentSession.objects.all()[0].name
        current_session = Year.objects.get(time=current_session1)
    except:
        current_session = None
        current_session1 = None



    if request.method == 'POST':
        news_search = request.POST['search']
        clas = request.POST['clas']
        if news_search:
            student = AcademicInfo.objects.filter(
                Q(registration_no__icontains=news_search)|
                Q(class_info__name__icontains=news_search)|
                Q(personal_info__first_name__icontains=news_search)|
                Q(personal_info__last_name__icontains=news_search)|
                Q(date__icontains=news_search)).filter(is_delete=False).order_by('-id')

        if len(student) > 0:

            paginator = Paginator(student, 10)
            page = request.GET.get('page')
            try:
                student = paginator.page(page)
            except PageNotAnInteger:
                student = paginator.page(1)
            except EmptyPage:
                student = paginator.page(paginator.num_pages)
        else:
            student = None
        context = {
            'result': result,
            'group_students':group_students,
            'student': student,
            'forms': forms,
            'class': clas,
            'forms1': forms1,
            'current_session': current_session,
            'current_term': current_term,
            'current_session1': current_session1,
            'current_term1': current_term1
            # 'img':response
        }
        #return render(request, 'student/student-list-django.html', context)
        if request.is_ajax():
            html = render_to_string('student/student-list_replace1.html', context, request=request)
            return JsonResponse({'form': html})
    context = {
        'result': result,
        'group_students':group_students,
        'student': student,
        'forms': forms,
        'forms1': forms1,
        'current_session': current_session,
        'current_term': current_term
        # 'img':response
    }
    return render(request, 'student/student-list.html', context)




@login_required(login_url='administration:login')
@permission_required('administration.add_result')
def change_password(request, username=None):
    user = User.objects.get(username=username)
    profi_info = PersonalInfo.objects.get(user=user)
    full_name = profi_info.first_name.capitalize() + ' ' + profi_info.last_name.capitalize()
    academic_info = AcademicInfo.objects.get(registration_no=username)
    clas = academic_info.class_info.name

    if request.method == 'POST' and request.is_ajax():
        personal_info = PasswordChange(instance=profi_info)
        form =PasswordChangeForm(user)


        context = {
            'form': form,
            'personal_info': personal_info,
            'full_name': full_name
        }
        html = render_to_string('student/password-change.html', context, request=request)
        return JsonResponse({'form': html})

    if request.method == 'POST':
        form = PasswordChangeForm(user, request.POST)
        if form.is_valid():
            profi_info.pass_word = request.POST['new_password1']
            user = form.save()
            profi_info.save()
            update_session_auth_hash(request, user)
            messages.success(request, "Password Changed Successfuly", extra_tags='success')
            return redirect('student-list-re', username=clas)
        else:
            messages.success(request, "Error, ensure both password are the same.", extra_tags='error')
            return redirect('student-list-re', username=clas)





@login_required(login_url='administration:login')
@permission_required('administration.add_result')
def student_edit(request, username):
    if request.method == 'POST' and request.is_ajax():
        user = User.objects.get(username=username)
        profi_info = PersonalInfo.objects.get(user=user)

        academic_info = AcademicInfo.objects.get(registration_no=username)


        academic_info_form = AcademicInfoEditForm(instance=academic_info)
        personal_info_form = PersonalInfoFormEdit(instance=profi_info, user=request.user)
        context = {
            'personal_info_form': personal_info_form,
            'academic_info_form': academic_info_form
        }
        html = render_to_string('student/student-info-edit.html', context, request=request)
        return JsonResponse({'form': html})

    if request.method == 'POST':
        user = User.objects.get(username=username)
        profi_info = PersonalInfo.objects.get(user=user)
        academic_info = AcademicInfo.objects.get(registration_no=username)
        clas = academic_info.class_info.name
        academic_info_form = AcademicInfoEditForm(data=request.POST, instance=academic_info)
        personal_info_form = PersonalInfoFormEdit(user=request.user,instance=profi_info, data=request.POST, files=request.FILES)

        if personal_info_form.is_valid() and academic_info_form.is_valid():
            academic_info_form.save()
            personal_info = personal_info_form.save(commit=False)
            personal_info.user = user
            personal_info.save()
            academic_info = AcademicInfo.objects.get(registration_no=username)
            academic_info.class_info = personal_info.class_of_student
            academic_info.save()
            #academic_info_form.class_info = ClassInfo.objects.get(class_info=)
            messages.success(request, "Student edited successfuly", extra_tags='success')
            return redirect('student-list-re', username=clas)
        else:
            messages.success(request, "Student not edited", extra_tags='error')
            return redirect('student-list-re', username=clas)


@login_required(login_url='administration:login')
@permission_required('administration.add_result')
def student_delete(request, username):
    user = User.objects.get(username=username)
    student = AcademicInfo.objects.get(registration_no=username)
    clas = student.class_info.name
    student.is_delete = True

    my_group = Group.objects.get(name='Students')
    my_group.user_set.remove(user)

    student.save()
    messages.success(request, "Student Deleted Temporarily", extra_tags='success')
    return redirect('student-list-re', username=clas)



@login_required(login_url='administration:login')
@permission_required('administration.delete_result')
def deleted_students(request, username=None):
    if username !=None:
        try:
            user = User.objects.get(username=username)
        except:
            user = None

        if user == None:
            messages.success(request, "Refresh page and try deleting again", extra_tags='error')
            return redirect('deleted-students')
        else:
            print(user)
            user.delete()
            messages.success(request, "Student Deleted Completely", extra_tags='success')
            return redirect('deleted-students')

    else:
        student = AcademicInfo.objects.filter(is_delete=True)
        if len(student) > 0:

            paginator = Paginator(student, 10)
            page = request.GET.get('page')
            try:
                student = paginator.page(page)
            except PageNotAnInteger:
                student = paginator.page(1)
            except EmptyPage:
                student = paginator.page(paginator.num_pages)
        else:
            student = None



    context = {
        'student': student
        # 'img':response
    }
    return render(request, 'student/deleted-student-list.html', context)

@login_required(login_url='administration:login')
@permission_required('teacher.delete_personal_info')
def restore_student_re(request, username):
    user = User.objects.get(username=username)
    personal_info = PersonalInfo.objects.get(user=user)
    student = AcademicInfo.objects.get(personal_info=personal_info)
    student.is_delete = False

    my_group = Group.objects.get(name='Students')
    my_group.user_set.add(user)


    student.save()
    clas = student.class_info.name
    messages.success(request, f"Student Restored To {clas.upper()} Successfuly", extra_tags='success')
    return redirect('deleted-students')




@permission_required('teacher.view_personal_info')
def teacher_list(request):
    group_students = []
    username = request.user
    user = User.objects.get(username=username)
    group_student = User.objects.filter(groups__name='Students')
    for i in group_student:
        group_students.append(i.username)
    username = request.user
    user = User.objects.get(username=username)
    try:
        result = Result.objects.filter(user=user)[0]
    except:
        result = None
    teacher = PersonalInfo.objects.filter(is_delete=False)
    context = {
        'result': result,
        'group_students':group_students,
        'teacher': teacher
        # 'img':response
    }


    return render(request, 'teacher/teacher-list.html', context)

@login_required(login_url='administration:login')
@permission_required('student.view_personal_info')
def student_profile(request, reg_no):
    student = AcademicInfo.objects.get(registration_no=reg_no)
    context = {
        'student': student
    }
    return render(request, 'student/student-profile.html', context)



@login_required(login_url='administration:login')
@permission_required('administration.add_result')
def result_delete(request, reg_no):

    student = AcademicInfo.objects.get(registration_no=reg_no)
    result = student.result
    user = student.personal_info.user
    clas_inf = student.class_info
    clas = ClassInfo.objects.get(name=clas_inf)


    try:
        current_term1 = CurrentTerm.objects.all()[0].name
        current_term = Term.objects.get(term=current_term1)
    except:
        current_term = None
        current_term1 = None


    try:
        current_session1 = CurrentSession.objects.all()[0].name
        current_session = Year.objects.get(time=current_session1)
    except:
        current_session = None
        current_session1 = None

    try:
        perform = Result.objects.get(clas=clas, user=user, term=current_term, year=current_session)


    except:
        perform = None

    if perform:
        student.result = None
        student.save()
        perform.delete()
        try:
            student = AcademicInfo.objects.filter(class_info=clas_inf).filter(is_delete=False).order_by('-id')
        except:
            student = None
        paginator = Paginator(student, 10)
        page = request.GET.get('page')
        try:
            student = paginator.page(page)
        except PageNotAnInteger:
            student = paginator.page(1)
        except EmptyPage:
            student = paginator.page(paginator.num_pages)
        forms = StudentSearchForm1(request.user)
        forms1 = ResultForm()
        context = {
            'student': student,
            'forms': forms,
            'class': clas,
            'forms1': forms1,
            'current_session1': current_session1,
            'current_session': current_session,
            'current_term1': current_term1,
            'current_term': current_term,
            # 'img':response
        }
        messages.success(request, "Result deleted Successfuly", extra_tags='success')
        return redirect('student-list-re', username=clas)
    else:
        try:
            student = AcademicInfo.objects.filter(class_info=clas_inf).filter(is_delete=False).order_by('-id')
        except:
            student = None
        paginator = Paginator(student, 10)
        page = request.GET.get('page')
        try:
            student = paginator.page(page)
        except PageNotAnInteger:
            student = paginator.page(1)
        except EmptyPage:
            student = paginator.page(paginator.num_pages)
        forms = StudentSearchForm1(request.user)
        forms1 = ResultForm()
        context = {
            'student': student,
            'forms': forms,
            'class': clas,
            'forms1': forms1,
            'current_session1': current_session1,
            'current_session': current_session,
            'current_term1': current_term1,
            'current_term': current_term,
            # 'img':response
        }
        messages.success(request, "No result to be deleted", extra_tags='error')
        return redirect('student-list-re', username=clas)





@login_required(login_url='administration:login')
@permission_required('administration.add_result')
def student_search(request, clas):
    group_students = []
    username = request.user
    user = User.objects.get(username=username)
    group_student = User.objects.filter(groups__name='Students')
    for i in group_student:
        group_students.append(i.username)
    try:
        result = Result.objects.filter(user=user)[0]
    except:
        result = None
    forms = StudentSearchForm1(request.user, request.POST)
    if request.method == 'POST':
        news_search = request.POST.get('search_inputs')
        if news_search:
            student = AcademicInfo.objects.filter(
                Q(registration_no__icontains=news_search)|
                Q(personal_info__first_name__icontains=news_search)|
                Q(personal_info__last_name__icontains=news_search)|
                Q(date__icontains=news_search)).filter(is_delete=False).order_by('-id')
            paginator = Paginator(student, 10)
            page = request.GET.get('page')
            try:
                student = paginator.page(page)
            except PageNotAnInteger:
                student = paginator.page(1)
            except EmptyPage:
                student = paginator.page(paginator.num_pages)
        forms = StudentSearchForm1(request.user)
        context = {
            'result': result,
            'group_students':group_students,
            'student': student,
            'forms': forms,
            'clas': clas
        }
        return render(request, 'student/student-list-django.html', context)

@login_required(login_url='administration:login')
def student_result(request, reg_no):
    academic_info = AcademicInfo.objects.get(registration_no=reg_no)

    user = academic_info.personal_info.user
    clas = academic_info.class_info.name


    try:
        current_term1 = CurrentTerm.objects.all()[0].name
        current_term = Term.objects.get(term=current_term1)
    except:
        current_term = None
        current_term1 = None


    try:
        current_session1 = CurrentSession.objects.all()[0].name
        current_session = Year.objects.get(time=current_session1)
    except:
        current_session = None
        current_session1 = None




    try:
        result = Result.objects.get(user=user, year=current_session, term=current_term)
    except:
        result = None


    url = '/student/student-result/' + reg_no+'/'



    if result != None:
        context = {
            'result': result,
            'clas': clas,
            'url': url,
            'reg_no': reg_no
        }
        return render(request, 'result/mark-entry1.html', context)
    else:
        messages.success(request, "Result not availabe", extra_tags='error')
        return redirect('student-list-re', username=clas)







@login_required(login_url='administration:login')
def home_page(request):
    # global response
    group_students = []
    username = request.user
    user = User.objects.get(username=username)
    group_student = User.objects.filter(groups__name='Students')
    for i in group_student:
        group_students.append(i.username)
    username = request.user
    user = User.objects.get(username=username)
    try:
        result = Result.objects.filter(user=user)[0]
    except:
        result = None
    context = {
        'result': result,
        'group_students':group_students,
        # 'img':response
    }
    #return render(request, 'administration/dashboard.html', context)
    return render(request, 'home.html', context)


@permission_required('administration.add_result')
def enrolled_student(request):
    group_students = []
    username = request.user
    user = User.objects.get(username=username)
    group_student = User.objects.filter(groups__name='Students')
    for i in group_student:
        group_students.append(i.username)
    username = request.user
    user = User.objects.get(username=username)
    try:
        result = Result.objects.filter(user=user)[0]
    except:
        result = None
    forms = EnrolledStudentForm()
    cls = request.GET.get('class_name', None)
    student = AcademicInfo.objects.filter(class_info=cls, status='not enroll')
    context = {
        'result': result,
        'group_students':group_students,
        'forms': forms,
        'student': student
        # 'img':response
    }
    #return render(request, 'administration/dashboard.html', context)
    return render(request, 'student/enrolled.html', context)


@permission_required('administration.add_result')
def student_enrolled(request, reg):
    student = AcademicInfo.objects.get(registration_no=reg)
    forms = StudentEnrollForm()
    if request.method == 'POST':
        forms = StudentEnrollForm(request.POST)
        if forms.is_valid():
            roll = forms.cleaned_data['roll_no']
            class_name = forms.cleaned_data['class_name']
            EnrolledStudent.objects.create(class_name=class_name, student=student, roll=roll)
            student.status = 'enrolled'
            student.save()
            return redirect('enrolled-student-list')
    context = {
        'student': student,
        'forms': forms
    }
    return render(request, 'student/student-enrolled.html', context)



@permission_required('administration.add_result')
def enrolled_student_list(request):
    student = EnrolledStudent.objects.all()
    forms = SearchEnrolledStudentForm()
    class_name = request.GET.get('reg_class', None)
    roll = request.GET.get('roll_no', None)
    if class_name:
        student = EnrolledStudent.objects.filter(class_name=class_name)
        context = {
            'forms': forms,
            'student': student
        }
        return render(request, 'student/enrolled-student-list.html', context)
    context = {
        'forms': forms,
        'student': student
    }
    return render(request, 'student/enrolled-student-list.html', context)
