from django.db import models
import random
from django.contrib.auth.models import User

from academic.models import ClassInfo, ClassRegistration
from address.models import District, Upazilla, Union
from django.core.validators import FileExtensionValidator
from administration.models import *



class PersonalInfo(models.Model):
    first_name = models.CharField(max_length=45, null=True, blank=True,)
    last_name = models.CharField(max_length=45, null=True, blank=True,)
    pass_word = models.CharField(max_length=45, null=True, blank=True,)
    class_of_student = models.ForeignKey(ClassInfo, default='', on_delete=models.CASCADE, related_name="class_of_students",)
    user = models.ForeignKey(User, default='', on_delete=models.CASCADE, related_name="user_student",)
    photo = models.ImageField(upload_to='student-photos/', null=True, blank=True,)
    blood_group_choice = (
        ('a+', 'A+'),
        ('o+', 'O+'),
        ('b+', 'B+'),
        ('ab+', 'AB+'),
        ('a-', 'A-'),
        ('o-', 'O-'),
        ('b-', 'B-'),
        ('ab-', 'AB-')
    )
    blood_group = models.CharField(choices=blood_group_choice, max_length=5, null=True, blank=True,)
    date_of_birth = models.DateField(null=True, blank=True,)
    gender_choice = (
        ('male', 'Male'),
        ('female', 'Female')
    )
    gender = models.CharField(choices=gender_choice, max_length=10, null=True, blank=True,)
    phone_no = models.CharField(max_length=11, null=True, blank=True,)
    email = models.EmailField(blank=True, null=True)
    birth_certificate_no = models.IntegerField(null=True, blank=True,)
    religion_choice = (
        ('Islam', 'Islam'),
        ('Christianity', 'Christianity')
    )
    religion = models.CharField(choices=religion_choice, max_length=45, null=True, blank=True,)
    nationality_choice = (
        ('Ghana', 'Ghana'),
        ('Nigeria', 'Nigeria')
    )
    nationality = models.CharField(choices=nationality_choice, max_length=45, null=True, blank=True,)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"






class AcademicInfo(models.Model):
    class_info = models.ForeignKey(ClassInfo, on_delete=models.CASCADE, blank=True, null=True, related_name="student_class")
    registration_no = models.CharField(max_length=100, unique=True, null=True, blank=True,)
    #     ('not enroll', 'Not Enroll'),
    #     ('enrolled', 'Enrolled'),
    #     ('regular', 'Regular'),
    #     ('irregular', 'Irregular'),
    #     ('passed', 'Passed'),
    # )
    # status = models.CharField(choices=status_select, default='not enroll', max_length=15, null=True, blank=True,)
    personal_info = models.ForeignKey(PersonalInfo, on_delete=models.CASCADE, null=True, blank=True, related_name="student_persona")
    result = models.ForeignKey(Result, on_delete=models.CASCADE, null=True, blank=True, related_name="student_result")
    # address_info = models.ForeignKey(StudentAddressInfo, on_delete=models.CASCADE, null=True, blank=True,)
    # guardian_info = models.ForeignKey(GuardianInfo, on_delete=models.CASCADE, null=True, blank=True,)
    # emergency_contact_info = models.ForeignKey(EmergencyContactDetails, on_delete=models.CASCADE, null=True, blank=True,)
    # previous_academic_info = models.ForeignKey(PreviousAcademicInfo, on_delete=models.CASCADE, null=True, blank=True,)
    # previous_academic_certificate = models.ForeignKey(PreviousAcademicCertificate, on_delete=models.CASCADE, null=True, blank=True,)
    date = models.DateField(auto_now_add=True)
    is_delete = models.BooleanField(default=False)

    def __str__(self):
        return str(self.registration_no)

class EnrolledStudent(models.Model):
    # class_name = models.ForeignKey(ClassRegistration, on_delete=models.CASCADE)
    class_name = models.ForeignKey(ClassInfo, on_delete=models.CASCADE)
    student = models.OneToOneField(AcademicInfo, on_delete=models.CASCADE)
    roll = models.IntegerField()
    date = models.DateField(auto_now_add=True)

    class Meta:
        unique_together = ['class_name', 'roll']

    def __str__(self):
        return str(self.roll)




class ExcelUpload(models.Model):
    excell_sheet = models.FileField(null=True, blank=True)


class Cert(models.Model):


    pdf = models.FileField(upload_to='files', blank=True, null=True)

