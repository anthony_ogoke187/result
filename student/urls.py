from django.urls import path, re_path
from . import views

urlpatterns = [
    path('class-wise-student-registration', views.class_wise_student_registration, name='class-wise-student-registration'),
    path('students-excel-format', views.export_students_registration_xls, name='export_students_registration_xls'),
    #re_path(r'student-registration/(?P<name_teacher>[\w\s\w]+)/$', views.student_registration, name="student-registration"),
    path('student-registration', views.student_registration, name='student-registration'),
    path('student-list', views.student_list, name='student-list'),
    path('teacher-upload/', views.excell_student_registration, name='excell_student_registration'),
    re_path(r'student-list/(?P<username>[\w/]+)/$', views.student_list, name='student-list-re'),
    path('student-list-search', views.student_list_search, name='student-list-search'),
    #path('student-list2', views.student_list2, name='student-list2'),
    #path('student-result', views.student_result, name='student-result'),
    re_path(r'student-result/(?P<reg_no>[\w/]+)/$', views.student_result, name="student-result"),

    re_path(r'student-edit-edit/(?P<username>[\w/]+)/$', views.student_edit, name="student-edit-edit"),
    re_path(r'student-delete/(?P<username>[\w/_]+)/$', views.student_delete, name="student-delete"),
    re_path(r'student-upload-result/(?P<reg_no>[\w/]+)$', views.student_upload_result, name="student-upload-result"),
    re_path(r'profile/(?P<reg_no>[\w/]+)/$', views.student_profile, name="student-profile"),
    re_path(r'restore-student-re/(?P<username>[\w/_]+)/$', views.restore_student_re, name="restore-student-re"),
    #path('edit/<reg_no>', views.student_edit, name='student-edit'),
    # re_path(r'edit/(?P<reg_no>[\w/]+)/$', views.student_edit, name="student-edit"),
    #path('delete/<reg_no>', views.student_delete, name='student-delete'),
    re_path(r'delete/(?P<reg_no>[\w/]+)/$', views.result_delete, name="result-delete"),
    re_path(r'student-search/(?P<clas>[\w]+)/$', views.student_search, name='student-search'),
    path('enrolled/', views.enrolled_student, name='enrolled-student'),
    path('deleted-students', views.deleted_students, name='deleted-students'),
    re_path(r'deleted-students/(?P<username>[\w/_]+)/$', views.deleted_students, name="deleted-students-re"),
    #path('enrolled-student/<reg>', views.student_enrolled, name='enrolled'),
    re_path(r'enrolled-student/(?P<reg>[\w/]+)/$', views.student_enrolled, name="enrolled"),
    path('enrolled-student-list/', views.enrolled_student_list, name='enrolled-student-list'),
]
