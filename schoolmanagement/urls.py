"""schoolmanagement URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path, include
from .views import home_page, preview
import teacher.views as views
import student.views as student_views
from . import settings
from django.contrib.staticfiles.urls import static, staticfiles_urlpatterns

import django.contrib.auth.views as auth_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', home_page, name='home'),
    path('preview/', preview, name='preview'),
    path('', include('administration.urls')),
    #re_path(r'classroom-delete/(?P<class_name>[\w/]+)/$', views.create_class_delete, name="classroom-delete"),
    path('teacher/', include('teacher.urls')),
    path('student/', include('student.urls')),
    path('academic/', include('academic.urls')),
    path('employee/', include('employee.urls')),
    path('result/', include('result.urls')),
    path('address/', include('address.urls')),
    path('account/', include('account.urls')),
    path('attendance/', include('attendance.urls')),
    re_path(r'password_change/(?P<username>[\w/]+)/$', student_views.change_password, name="password_change"),
    #path('password_change/', auth_views.PasswordChangeView.as_view(), name='password_change'),
    path('password_change/done/', auth_views.PasswordChangeDoneView.as_view(), name='password_change_done'),

    path('password_reset/', auth_views.PasswordResetView.as_view(), name='password_reset'),
    path('password_reset/done/', auth_views.PasswordResetDoneView.as_view(), name='password_reset_done'),
    path('reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('reset/done/', auth_views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),
]
urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
