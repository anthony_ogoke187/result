from django.shortcuts import render
from django.contrib.auth.models import User, Group
from django.contrib.auth.decorators import login_required
import student
import teacher
import employee
import academic
from administration.models import *

from student.models import AcademicInfo

import logging


logger = logging.getLogger(__name__)



@login_required(login_url='administration:login')
def home_page(request):
    try:
        class_assigned = request.user.classes_assigned_teacher.all()
    except:
        class_assigned = None
        total_student_class = None
    if class_assigned != None:
        total_student_class = {}
        total_selected_class = []
        for i in class_assigned:
            class_name = i.name

            class_info = ClassInfo.objects.get(name=class_name)

            stud = AcademicInfo.objects.filter(class_info=class_info).filter(is_delete=False).count()
            total_student_class[class_name] = stud
            print(total_student_class)
            total_selected_class.append(int(stud))
        all_student_class = sum(total_selected_class)
    total_student = student.models.AcademicInfo.objects.filter(is_delete=False).count()
    total_teacher= teacher.models.PersonalInfo.objects.filter(is_delete=False).count()
    total_employee = employee.models.PersonalInfo.objects.count()
    total_class = academic.models.ClassInfo.objects.count()
    context = {
        'student': total_student,
        'teacher': total_teacher,
        'employee': total_employee,
        'total_class': total_class,
        'total_student_class': total_student_class,
        'all_student_class': all_student_class
    }
    return render(request, 'home.html', context)
    # return render(request, 'home.html', context)

@login_required(login_url='administration:login')
def mark_entry(request):
    # global response

    group_students = []
    username = request.user
    user = User.objects.get(username=username)
    group_student = User.objects.filter(groups__name='Students')
    print(group_student)
    group_admin = Group.objects.get(name="Admin")
    for i in group_student:
        group_students.append(i.username)
        print(i)
    username = request.user
    user = User.objects.get(username=username)
    try:
        result = Performance.objects.filter(user=user)[0]
    except:
        result = None
    
    form = AdministrationForm()
    if request.is_ajax and request.method == "POST":
        # html = render_to_string('rscheck/table.html', context, request=request)
        html = render_to_string('result/mark-entry1.html', request=request)
        return JsonResponse({'form': html})

    context = {
        'result': result,
        'form': form,
        'group_student':group_student,
        'group_students':group_students,
        # 'img':response
    }
    #return render(request, 'administration/dashboard.html', context)
    return render(request, 'result/mark-entry.html', context)


@login_required(login_url='administration:login')
def home_page1(request):
    total_student = Group.objects.all().count()
    print(total_student)
    username = request.user
    user = User.objects.get(username=username)
    try:
        result = Performance.objects.filter(user=user)[0]
    except:
        result = None
    context = {
        'result': result,
        # 'img':response
    }
    #return render(request, 'administration/dashboard.html', context)
    return render(request, 'home.html', context)





def preview(request):
    posts = Post.objects.all()
    response_data = {}

    if request.POST.get('action') == 'post':
        title = request.POST.get('title')
        description = request.POST.get('description')

        response_data['title'] = title
        response_data['description'] = description

        Post.objects.create(
            title = title,
            description = description,
            )
        return JsonResponse(response_data)

    return render(request, 'create_post.html', {'posts':posts}) 