from django.urls import path, re_path

from . import views

urlpatterns = [
    # path('add-department', views.add_department, name='add-department'),
    #path('create-class', views.add_class, name='create-class'),
    # path('create-section', views.create_section, name='create-section'),
    path('create-session', views.create_session, name='create-session'),
    path('current-session', views.current_session, name='current-session'),
    re_path(r'session-delete/(?P<session1>[\w/]+)/$', views.session_delete, name="session-delete"),
    path('create-term', views.create_term, name='create-term'),
    path('current-term', views.current_term, name='current-term'),
    re_path(r'term-delete/(?P<term>[\w|\W]+)/$', views.term_delete, name="term-delete"),
    path('class-registration', views.class_registration, name='class-registration'),
    path('guide-teacher', views.create_guide_teacher, name='guide-teacher'),
    path('class-list', views.class_list, name='class-list'),
]
