from django import forms
from .models import *
from academic.models import ClassRegistration
from administration.models import *



class SessionForm(forms.ModelForm):
    class Meta:
        model = Year
        fields = '__all__'
        widgets = {
            'time': forms.TextInput(attrs={'class': 'form-control'}),
        }

class TermForm(forms.ModelForm):
    class Meta:
        model = Term
        fields = '__all__'
        widgets = {
            'term': forms.TextInput(attrs={'class': 'form-control'}),
        }


class CurrentSessionForm(forms.ModelForm):
    class Meta:
        model = CurrentSession
        fields = (
            'name',
            )


class CurrentTermForm(forms.ModelForm):
    class Meta:
        model = CurrentTerm
        fields = (
            'name',
            )



class GuideTeacherForm(forms.ModelForm):
    class Meta:
        model = GuideTeacher
        fields = '__all__'
        widgets = {
            'name': forms.Select(attrs={'class': 'form-control'}),
        }
