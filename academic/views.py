from django.shortcuts import render, redirect
from .forms import *
from .models import *
from academic.models import ClassRegistration

from django.contrib.auth.models import User, Group
from django.contrib.auth.decorators import login_required
from administration.models import *

import logging



@login_required(login_url='administration:login')
def create_session(request):
    forms = SessionForm()
    if request.method == 'POST':
        forms = SessionForm(request.POST)
        if forms.is_valid():
            forms.save()
            return redirect('create-session')
    session = Year.objects.all().order_by('id')
    context = {
        'forms': forms,
        'session': session
    }
    return render(request, 'academic/create-session.html', context)



@login_required(login_url='administration:login')
def current_session(request):
    forms = CurrentSessionForm()
    if request.method == 'POST':
        forms = CurrentSessionForm(request.POST)
        if forms.is_valid():
            current_session = CurrentSession.objects.all().order_by('id')
            for i in current_session:
                i.delete()
            forms.save()
            return redirect('current-session')
    session = Year.objects.all().order_by('id')
    current_session = CurrentSession.objects.all().order_by('id')
    context = {
        'forms': forms,
        'current_session': current_session,
        'session': session
    }
    return render(request, 'academic/current-session.html', context)



@login_required(login_url='administration:login')
def session_delete(request, session1):
    session_obj = Year.objects.get(time=session1)
    try:
        current_session = CurrentSession.objects.get(term=session1)
    except:
        current_session = None
    if current_session != None:
        current_session.name = None
    session_obj.delete()
    return redirect('create-session')




@login_required(login_url='administration:login')
def create_term(request):
    forms = TermForm()
    if request.method == 'POST':
        forms = TermForm(request.POST)
        if forms.is_valid():
            forms.save()
            return redirect('create-term')
    term = Term.objects.all().order_by('id')
    context = {
        'forms': forms,
        'term': term
    }
    return render(request, 'academic/create-term.html', context)



@login_required(login_url='administration:login')
def current_term(request):
    forms = CurrentTermForm()
    if request.method == 'POST':
        forms = CurrentTermForm(request.POST)
        if forms.is_valid():
            current_term = CurrentTerm.objects.all().order_by('id')
            for i in current_term:
                i.delete()
            forms.save()
            return redirect('current-term')
    term = Term.objects.all().order_by('id')
    current_term = CurrentTerm.objects.all().order_by('id')
    context = {
        'forms': forms,
        'current_term': current_term,
        'term': term
    }
    return render(request, 'academic/current-term.html', context)





@login_required(login_url='administration:login')
def term_delete(request, term):
    term_obj = Term.objects.get(term=term)
    try:
        current_term = CurrentTerm.objects.get(term=term)
    except:
        current_term = None
    if current_term != None:
        current_term.name = None
    term_obj.delete()
    return redirect('create-term')



@login_required(login_url='administration:login')
def class_list(request):
    register_class = ClassRegistration.objects.all()
    context = {'register_class': register_class}
    return render(request, 'academic/class-list.html', context)



@login_required(login_url='administration:login')
def class_registration(request):
    forms = ClassRegistrationForm()
    if request.method == 'POST':
        forms = ClassRegistrationForm(request.POST)
        if forms.is_valid():
            forms.save()
            return redirect('class-list')
    context = {'forms': forms}
    return render(request, 'academic/class-registration.html', context)






@login_required(login_url='administration:login')
def create_guide_teacher(request):
    forms = GuideTeacherForm()
    if request.method == 'POST':
        forms = GuideTeacherForm(request.POST)
        if forms.is_valid():
            forms.save()
            return redirect('guide-teacher')
    guide_teacher = GuideTeacher.objects.all()
    context = {
        'forms': forms,
        'guide_teacher': guide_teacher
    }
    return render(request, 'academic/create-guide-teacher.html', context)
